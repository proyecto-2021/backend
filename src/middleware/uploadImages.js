import multer from 'multer'

export const uploadInMemoryImages = ({ type = 'single', fieldName = '' }) => (
  req,
  res,
  next
) => {

  console.log('first middleware')

  const fileFilter = (req, file, cb) => {
    if (file.mimetype.split('/')[0] === 'image') {
      cb(null, true)
    } else {
      cb(null, false)
      return cb(new Error('Only images format allowed'))
    }
  }

  const limits = { fieldSize: 25 * 1024 * 1024 }

  let upload = null

  // return upload
  if (type === 'single') {
    upload = multer({
      storage: multer.memoryStorage(),
      limits
    }).single(fieldName)
  } else {
    upload = multer({
      storage: multer.memoryStorage(),
      limits
    }).any()
  }

  upload(req, res, function (err) {
    if (err instanceof multer.MulterError) {
      req.body.error = err
    } else if (err) {
      req.body.error = err
    }
    next()
  })
}
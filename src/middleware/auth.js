import { message } from '@config/message'
import jwt from 'jsonwebtoken'
import { handleError, handleResponse } from './errorHandlers'

export const authUser = (req, res, next) => {
  try {
    const bearerHeader = req.headers.authorization
    if (!req.headers.authorization) {
      return handleResponse(res, 400, 'access denied')
    }

    const bearer = bearerHeader.split(' ')
    const bearerToken = bearer[1]

    jwt.verify(bearerToken, 'r=2%P%25TDMNBaC6', (err, data) => {
      if (err) {
        handleResponse(res, 403, 'invalid token')
      } else {
        const token = jwt.decode(bearerToken)
        req.token = token
        next()
      }
    })
  } catch (error) {
    handleError(error, res)
  }
}

export const authAdmin = (req, res, next) => {
  try {
    const bearerHeader = req.headers.authorization
    if (!req.headers.authorization) {
      return handleResponse(res, 400, 'access denied')
    }

    const bearer = bearerHeader.split(' ')
    const bearerToken = bearer[1]

    jwt.verify(bearerToken, 'r=2%P%25TDMNBaC6', (err, data) => {
      if (err) {
        handleResponse(res, 403, 'invalid token')
      } else {
        const token = jwt.decode(bearerToken)

        if (token.type === 'A') {
          req.token = token
          next()
        } else {
          handleResponse(res, 401, message.unauthorized)
        }
      }
    })
  } catch (error) {
    handleError(error, res)
  }
}

export const authWizard = (req, res, next) => {
  try {
    const bearerHeader = req.headers.authorization
    if (!req.headers.authorization) {
      return handleResponse(res, 400, 'access denied')
    }

    const bearer = bearerHeader.split(' ')
    const bearerToken = bearer[1]

    jwt.verify(bearerToken, 'r=2%P%25TDMNBaC6', (err, data) => {
      if (err) {
        handleResponse(res, 403, 'invalid token')
      } else {
        const token = jwt.decode(bearerToken)

        if (['A', 'W'].includes(token.type)) {
          req.token = token
          next()
        } else {
          handleResponse(res, 401, message.unauthorized)
        }
      }
    })
  } catch (error) {
    handleError(error, res)
  }
}

export const authAdminOrCollector = (req, res, next) => {
  try {
    const bearerHeader = req.headers.authorization
    if (!req.headers.authorization) {
      return handleResponse(res, 400, 'access denied')
    }

    const bearer = bearerHeader.split(' ')
    const bearerToken = bearer[1]

    jwt.verify(bearerToken, 'r=2%P%25TDMNBaC6', (err, data) => {
      if (err) {
        handleResponse(res, 403, 'invalid token')
      } else {
        const token = jwt.decode(bearerToken)

        if (['A', 'CO'].includes(token.type)) {
          req.token = token
          next()
        } else {
          handleResponse(res, 401, message.unauthorized)
        }
      }
    })
  } catch (error) {
    handleError(error, res)
  }
}

import { v4 as uuidv4 } from 'uuid'
import path from 'path'
import { format } from 'util'
import { Storage } from '@google-cloud/storage'
import { handleError } from '@middleware/errorHandlers'

const storage = new Storage({
  projectId: 'agatha-e209d',
  keyFilename: './agatha.json'
})

const bucket = storage.bucket('agatha-e209d.appspot.com')

export const deleteGCSFile = async (fileNames = []) => {
  try {
    for (const f of fileNames) {
      const file = bucket.file(f.split('agatha-e209d.appspot.com/')[1])
      await file.delete()
    }
  } catch (error) {
    console.log('error delete GCSFile', error)
  }
}

export const sendUploadsToGCS =
  (folder = '') =>
  async (req, res, next) => {
    try {
      if (!req.file && !req.files.length > 0) {
        return next()
      }

      let promises = []

      const arrayImages = req.files || [req.file]

      for (const [index, image] of arrayImages.entries()) {
        const gcsname =
          uuidv4() + path.extname(image.originalname).toLocaleLowerCase()

        const file = bucket.file(`${folder}` + gcsname)
        const stream = file.createWriteStream({
          metadata: {
            contentType: image.mimetype
          },
          resumable: false
        })

        stream.on('error', (err) => {
          image.cloudStorageError = err
          next(err)
        })

        stream.end(image.buffer)

        promises.push(
          new Promise((resolve, reject) => {
            stream.on('finish', () => {
              console.log('iteracion')

              const publicUrl = format(
                `https://storage.googleapis.com/${bucket.name}/${file.name}`
              )
              arrayImages[index].url = publicUrl

              // try {
              // Make the file public
              bucket
                .file(`${folder}` + gcsname)
                .makePublic()
                .then(() => resolve())
              // } catch {
              //   return res.status(500).send({
              //     message: `Uploaded the file successfully: ${image.originalname}, but public access is denied!`,
              //     url: publicUrl
              //   })
              // }
            })
          })
        )
      }
      await Promise.all(promises)
      next()
    } catch (error) {
      handleError(error, res)
    }
  }

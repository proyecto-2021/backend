import { Router } from 'express'
import {
  deleteClassificationReport,
  findClassificationReportsByCreature,
  findClassificationReportById,
  listClassificationReports,
  registerClassificationReport,
  updateClassificationReport
} from './controller'
import {
  creatureIdClassificationReportExistRequest,
  classificationReportRequest,
  classificationReportExistRequest,
  verifyUserInClassificationReport
} from './middleware/validators'

const router = Router()

router.get('/', verifyUserInClassificationReport ,listClassificationReports)
router.get('/:id', [verifyUserInClassificationReport ,classificationReportExistRequest], findClassificationReportById)
router.get('/creature/:id',  [verifyUserInClassificationReport,creatureIdClassificationReportExistRequest], findClassificationReportsByCreature)

router.post('/', [verifyUserInClassificationReport,classificationReportRequest], registerClassificationReport)
router.put(
  '/:id',
  [ verifyUserInClassificationReport ,classificationReportExistRequest, classificationReportRequest],
  updateClassificationReport
)
router.delete('/:id', [verifyUserInClassificationReport,classificationReportExistRequest], deleteClassificationReport)

export const classificationReportRoutes = router
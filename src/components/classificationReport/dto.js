export const classificationReportResource = (resource) => ({
    id: resource.id,
    creatureId: resource.creatureId,
    creature: resource.creature.username,
    name: resource.name,
    type: resource.type,
    score: resource.score,
    isActive: resource.isActive
  })
  
  export const classificationReportResourceAlternative = (resource) => ({
    id: resource.id,
    creatureId: resource.creatureId,
    creature: resource.creature.dataValues.username,
    name: resource.name,
    type: resource.type,
    score: resource.score,
    isActive: resource.isActive
  })
  
  export const classificationReportResourceWithoutCreature = (resource) => ({
    id: resource.id,
    creatureId: resource.creatureId,
    name: resource.name,
    type: resource.type,
    score: resource.score,
    isActive: resource.isActive
  })
  
  export const classificationReportsResource = (resources) =>
    resources.map((resource) => classificationReportResource(resource))
  
  export const classificationReportsResourceAlternative = (resources) =>
    resources.map((resource) => classificationReportResourceAlternative(resource))
  
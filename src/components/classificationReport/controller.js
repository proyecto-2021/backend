import { message } from '@config/message'
import { handleError, handleResponse } from '@middleware/errorHandlers'
import {
  addClassificationReport,
  deleteClassificationReportById,
  getAllClassificationReports,
  getAllClassificationReportsByCreature,
  getClassificationReportById,
  updateClassificationReportById
} from './dao'
import {
  classificationReportResource,
  classificationReportResourceWithoutCreature,
  classificationReportsResource,
  classificationReportsResourceAlternative
} from './dto'

// /v1/classificationReports
export const listClassificationReports = async (req, res) => {
  try {
    const data = await getAllClassificationReports()
    handleResponse(
      res,
      200,
      message.success_long,
      classificationReportsResource(data)
    )
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/classificationReports/{id}:
export const findClassificationReportById = async (req, res) => {
  try {
    const data = await getClassificationReportById(req.params.id)

    handleResponse(
      res,
      200,
      message.success_long,
      classificationReportResource(data)
    )
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/classificationReports/creature/{id}:
export const findClassificationReportsByCreature = async (req, res) => {
  try {
    const data = await getAllClassificationReportsByCreature(req.params.id)

    handleResponse(
      res,
      200,
      message.success_long,
      classificationReportsResourceAlternative(data)
    )
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/classificationReports
export const registerClassificationReport = async (req, res) => {
  try {
    const classificationReport = req.body

    const data = await addClassificationReport(classificationReport)
    console.log(data)

    handleResponse(
      res,
      200,
      message.create_success,
      classificationReportResourceWithoutCreature(data)
    )
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/classificationReports/{id}:
export const updateClassificationReport = async (req, res) => {
  try {
    const id = req.params.id
    const classificationReport = req.body

    const data = await updateClassificationReportById(id, classificationReport)

    handleResponse(res, 200, message.update, classificationReportResource(data))
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/classificationReports/{id}:
export const deleteClassificationReport = async (req, res) => {
  try {
    const id = req.params.id
    const classificationReport = req.body

    //const data = await updateClassificationReportById(id, classificationReport)
    const data = await deleteClassificationReportById(req.params.id)
    handleResponse(res, 200, message.update, classificationReportResource(data))
  } catch (error) {
    handleError(error, res)
  }
}

import { check, validationResult } from 'express-validator'
import { message } from '@config/message'
import { handleResponse } from '@middleware/errorHandlers'

import { getCreatureById } from '@components/creature/dao'
import { getClassificationReportById } from '../dao'
import { verifyToken } from '@services/jwt'

const message_request = 'es requerido'


export const verifyUserInClassificationReport = async (req, res, next) => {
  const { type } = verifyToken(req, res)
  const { method } = req

  switch (type) {
    case 'A':
      next()
      break
    case 'C':
      next()
      break
    case 'W':
      if (method === 'GET') {
        next()
      } else {
        handleResponse(res, 403, message.forbidden)
      }
      break
    default:
      handleResponse(res, 403, message.forbidden_long)
      break
  }
}

export const classificationReportRequest = async (req, res, next) => {
  await check('creatureId', `creatureId ${message_request}`).notEmpty().run(req)
  await check('name', `name ${message_request}`).notEmpty().run(req)
  await check('type', `type ${message_request}`).notEmpty().run(req)
  await check('score', `score ${message_request}`).notEmpty().run(req)

  const request = validationResult(req)
  if (!request.isEmpty()) {
    return res.status(422).json(request)
  } else {
    next()
  }
}

export const classificationReportExistRequest = async (req, res, next) => {
  const classificationReport = await getClassificationReportById(req.params.id)

  if (!classificationReport) {
    handleResponse(res, 400, message.not_found_long)
  } else {
    next()
  }
}

export const creatureIdClassificationReportExistRequest = async (req, res, next) => {
  const creature = await getCreatureById(req.params.id)
  if (!creature) {
    handleResponse(res, 400, message.not_found_long)
  } else {
    next()
  }
}

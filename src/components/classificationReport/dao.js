import { CreatureModel } from '@services/database'
import { ClassificationReportModel } from '@services/database'

export const getAllClassificationReports = async () => {
  try {
    return await ClassificationReportModel.findAll({
      include: {
        model: CreatureModel,
        as: 'creature'
      }
    })
  } catch (error) {
    throw error
  }
}
//xasa
export const getAllClassificationReportsByCreature = async (creatureId) => {
  try {
    return await ClassificationReportModel.findAll({
      include: {
        model: CreatureModel,
        as: 'creature'
      },
      where: {
        creatureId
      }
    })
  } catch (error) {
    throw error
  }
}

export const getClassificationReportById = async (id) => {
  try {
    return await ClassificationReportModel.findByPk(id, {
      include: {
        model: CreatureModel,
        as: 'creature'
      }
    })
  } catch (error) {
    throw error
  }
}

export const addClassificationReport = async (classificationReport) => {
  try {
    return await ClassificationReportModel.create(classificationReport)
  } catch (error) {
    throw error
  }
}

export const updateClassificationReportById = async (id, classificationReport) => {
  try {
    const { name, type, score } = classificationReport
    const ClassificationReport = await ClassificationReportModel.findByPk(id, {
      include: {
        model: CreatureModel,
        as: 'creature'
      }
    })
    return ClassificationReport.update({ name, type, score })
  } catch (error) {
    throw error
  }
}

export const deleteClassificationReportById = async (id) => {
  try {
    const ClassificationReport = await ClassificationReportModel.findByPk(id, {
      include: {
        model: CreatureModel,
        as: 'creature'
      }
    })
    return ClassificationReport.update({ isActive: false })
  } catch (error) {
    throw error
  }
}
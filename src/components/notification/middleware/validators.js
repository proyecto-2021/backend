import { check, validationResult } from 'express-validator'
import { message } from '@config/message'
import { handleResponse } from '@middleware/errorHandlers'

import { getNotificationById } from '../dao'
import { getCollectorById } from '@components/collector/dao'

const message_request = 'es requerido'

export const notificationRequest = async (req, res, next) => {
  await check('collectorId', `collectorId ${message_request}`).notEmpty().run(req)
  await check('name', `name ${message_request}`).notEmpty().run(req)
  await check('description', `description ${message_request}`).notEmpty().run(req)

  const request = validationResult(req)
  if (!request.isEmpty()) {
    return res.status(422).json(request)
  } else {
    next()
  }
}

export const notificationExistRequest = async (req, res, next) => {
  const notification = await getNotificationById(req.params.id)
  if (!notification) {
    handleResponse(res, 400, message.not_found_long)
  } else {
    next()
  }
}

export const collectorIdNotificationExistRequest = async (req, res, next) => {
  const collectorId = req.params.collectorId || req.body.collectorId
  const collector = await getCollectorById(collectorId)
  if (!collector) {
    handleResponse(res, 400, `collectorId: ${message.not_found_long}`)
  } else {
    next()
  }
}
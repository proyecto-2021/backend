import { message } from '@config/message'
import { handleError, handleResponse } from '@middleware/errorHandlers'
import {
  addNotification,
  deleteNotificationById,
  getAllNotifications,
  getAllNotificationsByCollectorId,
  getNotificationById,
  updateNotificationById
} from './dao'
import {
  notificationResource,
  notificationResourceWithoutCollector,
  notificationsResource,
  notificationsResourceAlternative
} from './dto'

// /v1/notifications
export const listNotifications = async (req, res) => {
  try {
    const data = await getAllNotifications()
    handleResponse(res, 200, message.success_long, notificationsResource(data))
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/notifications/{id}:
export const findNotificationsById = async (req, res) => {
  try {
    const data = await getNotificationById(req.params.id)

    handleResponse(res, 200, message.success_long, notificationResource(data))
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/notifications/collector/{id}:
export const findNotificationsByCollector = async (req, res) => {
  try {
    const data = await getAllNotificationsByCollectorId(req.params.id)

    handleResponse(
      res,
      200,
      message.success_long,
      notificationsResourceAlternative(data)
    )
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/notifications
export const registerNotification = async (req, res) => {
  try {
    const notification = req.body

    const data = await addNotification(notification)

    handleResponse(
      res,
      200,
      message.create_success,
      notificationResourceWithoutCollector(data)
    )
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/notifications/{id}:
export const updateNotification = async (req, res) => {
  try {
    const id = req.params.id
    const notification = req.body

    const data = await updateNotificationById(id, notification)

    handleResponse(res, 200, message.update, notificationResource(data))
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/notifications/{id}:
export const deleteNotification = async (req, res) => {
  try {
    const id = req.params.id
    //const classificationReport = req.body

    //const data = await updateClassificationReportById(id, classificationReport)
    const data = await deleteNotificationById(req.params.id)
    handleResponse(
      res,
      200,
      message.delete,
      notificationResourceWithoutCollector(data)
    )
  } catch (error) {
    handleError(error, res)
  }
}

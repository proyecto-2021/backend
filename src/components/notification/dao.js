import { CollectorModel } from '@services/database'
import { NotificationModel } from '@services/database'

export const getAllNotifications = async () => {
  try {
    return await NotificationModel.findAll({
      include: [
        {
          model: CollectorModel,
          as: 'collector'
        }
      ]
    })
  } catch (error) {
    throw error
  }
}

export const getAllNotificationsByCollectorId = async (collectorId) => {
  try {
    return await NotificationModel.findAll({
      include: [
        {
          model: CollectorModel,
          as: 'collector'
        }
      ],
      where: {
        collectorId
      },
      order: [['id', 'DESC']]
    })
  } catch (error) {
    throw error
  }
}

export const getNotificationById = async (id) => {
  try {
    return await NotificationModel.findByPk(id, {
      include: [
        {
          model: CollectorModel,
          as: 'collector'
        }
      ]
    })
  } catch (error) {
    throw error
  }
}

export const addNotification = async (notification) => {
  try {
    return await NotificationModel.create(notification)
  } catch (error) {
    throw error
  }
}

export const updateNotificationById = async (id, notification) => {
  try {
    const { name, description, date, isActive } = notification
    const Notification = await NotificationModel.findByPk(id, {
      include: [
        {
          model: CollectorModel,
          as: 'collector'
        }
      ]
    })
    return Notification.update({
      name,
      description,
      date,
      isActive
    })
  } catch (error) {
    throw error
  }
}

export const deleteNotificationById = async (id) => {
  try {
    const data = await Notification.destroy({
      where: {
        id
      }
    })
    return data
  } catch (error) {
    throw error
  }
}

export const notificationResource = (resource) => ({
  id: resource.id,
  collector: resource.collector.username || resource.collectorId,
  name: resource.name,
  description: resource.description,
  date: resource.date,
  isActive: resource.isActive
})

export const notificationResourceAlternative = (resource) => ({
  id: resource.id,
  collector: resource.collector.dataValues.username,
  name: resource.name,
  description: resource.description,
  date: resource.date,
  isActive: resource.isActive
})

export const notificationResourceWithoutCollector = (resource) => ({
  id: resource.id,
  collectorId: resource.collectorId,
  name: resource.name,
  description: resource.description,
  date: resource.date,
  isActive: resource.isActive
})

export const notificationsResource = (resources) =>
  resources.map((resource) => notificationResource(resource))

export const notificationsResourceAlternative = (resources) =>
  resources.map((resource) => notificationResourceAlternative(resource))

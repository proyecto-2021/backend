import { collectorExistRequest } from '@components/collector/middleware/validators'
import { Router } from 'express'
//import { noExtendRight } from 'sequelize/types/lib/operators'
import {
  deleteNotification,
  findNotificationsByCollector,
  findNotificationsById,
  listNotifications,
  registerNotification,
  updateNotification
} from './controller'
import {
  collectorIdNotificationExistRequest,
  notificationExistRequest,
  notificationRequest
  //verifyUserInCreatureVersion
} from './middleware/validators'

const router = Router()
router.get('/', listNotifications)
router.get('/:id', [notificationExistRequest], findNotificationsById)
router.get(
  '/collector/:id',
  [collectorExistRequest],
  findNotificationsByCollector
)
router.post(
  '/',
  [notificationRequest, collectorIdNotificationExistRequest],
  registerNotification
)
router.put(
  '/:id',
  [notificationExistRequest, notificationRequest],
  updateNotification
)
router.delete('/:id', [notificationExistRequest], deleteNotification)

export const notificationRoutes = router

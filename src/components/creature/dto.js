import { getLastCreatureVersionById } from '@components/creatureVersion/dao'
import { userNoPasswordResource } from '@components/user/dto'

export const creatureResource = async (resource) => ({
  id: resource.id,
  description: resource.description,
  life: resource.life,
  avatar: resource.avatar,
  lat: resource.lat,
  lgn: resource.lgn,
  lastVersion:
    (await getLastCreatureVersionById(resource.id))[0]?.dataValues || null,
  category: resource.category,
  user: userNoPasswordResource(resource.user)
})

export const creatureResourceWithoutUser = (resource) => ({
  id: resource.id,
  username: resource.username,
  description: resource.description,
  life: resource.life,
  avatar: resource.avatar,
  categoryId: resource.categoryId,
  lat: resource.lat,
  lgn: resource.lgn
})

export const creaturesResource = async (resources) =>
  await Promise.all(
    resources.map(async (resource) => await creatureResource(resource))
  )

import { check, validationResult } from 'express-validator'
import { message } from '@config/message'
import { handleError, handleResponse } from '@middleware/errorHandlers'

import { getCreatureById, getCreatureByUsername } from '../dao'
import { getCategoryById } from '@components/category/dao'

const message_request = 'es requerido'

export const verifyUserInCreature = async (req, res, next) => {
  const { id, type, username, creatureId } = req.token
  const { method } = req

  switch (type) {
    case 'A':
      next()
      break
    case 'C':
      req.params?.id == id || req.params?.username === username
        ? next()
        : handleResponse(res, 403, message.forbidden_long)
      break
    case 'W':
      if (method === 'GET') {
        req.params?.id == creatureId
          ? next()
          : handleResponse(res, 403, message.forbidden_long)
      } else {
        handleResponse(res, 403, message.forbidden_long)
      }
      break
    default:
      method === 'GET'
        ? next()
        : handleResponse(res, 403, message.forbidden_long)
      break
  }
}

export const creatureRequest = async (req, res, next) => {
  await check('username', `username ${message_request}`).notEmpty().run(req)

  const request = validationResult(req)
  if (!request.isEmpty()) {
    return res.status(422).json(request)
  } else {
    next()
  }
}

export const updatecreatureRequest = async (req, res, next) => {
  await check('description', `description ${message_request}`)
    .notEmpty()
    .run(req)
  // await check('avatar', `avatar ${message_request}`).notEmpty().run(req)

  const request = validationResult(req)
  if (!request.isEmpty()) {
    return res.status(422).json(request)
  } else {
    next()
  }
}

export const creatureExistRequest = async (req, res, next) => {
  const creature = await getCreatureById(req.params.id)

  if (!creature) {
    handleResponse(res, 400, message.not_found_long)
  } else {
    next()
  }
}

export const usernameCreatureExistRequest = async (req, res, next) => {
  const creature = await getCreatureByUsername(req.params.username)

  if (!creature) {
    handleResponse(res, 400, message.not_found_long)
  } else {
    next()
  }
}

export const validateCategoryExistRequest = async (req, res, next) => {
  try {
    const category = await getCategoryById(req.body.categoryId || -1)

    if (!category && req.body.categoryId) {
      handleResponse(res, 400, `Categoria: ${message.not_found_long}`)
    } else {
      next()
    }
  } catch (error) {
    handleError(error, res)
  }
}

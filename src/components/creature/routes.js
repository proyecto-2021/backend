import { userValidators } from '@components/auth/routes'
import { sendUploadsToGCS } from '@middleware/uploadGCS'
import { uploadInMemoryImages } from '@middleware/uploadImages'
import { Router } from 'express'
import {
  findCreatureByUsername,
  findCreatureById,
  listCreatures,
  registerCreature,
  updateCreature,
  updateAvatarCreature
} from './controller'
import {
  creatureExistRequest,
  creatureRequest,
  updatecreatureRequest,
  usernameCreatureExistRequest,
  validateCategoryExistRequest,
  verifyUserInCreature
} from './middleware/validators'

const router = Router()

router.get('/', verifyUserInCreature, listCreatures)
router.get(
  '/:id',
  [verifyUserInCreature, creatureExistRequest],
  findCreatureById
)
router.get(
  '/user/:username',
  [verifyUserInCreature, usernameCreatureExistRequest],
  findCreatureByUsername
)
router.post(
  '/',
  uploadInMemoryImages({ type: 'any' }),
  userValidators,
  [verifyUserInCreature, creatureRequest, validateCategoryExistRequest],
  sendUploadsToGCS(),
  registerCreature
)
router.put(
  '/:id',
  uploadInMemoryImages({ type: 'any' }),
  [
    verifyUserInCreature,
    creatureExistRequest,
    updatecreatureRequest,
    validateCategoryExistRequest
  ],
  sendUploadsToGCS(),
  updateCreature
)

router.put(
  '/avatar/:id',
  [verifyUserInCreature, creatureExistRequest],
  updateAvatarCreature
)

export const creatureRoutes = router

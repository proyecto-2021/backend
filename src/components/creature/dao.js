import {
  CategoryModel,
  ClassificationReportModel,
  CreatureModel,
  CreatureVersionModel,
  db,
  FavoriteModel,
  ReportModel,
  UserModel
} from '@services/database'

export const getAllCreatures = async () => {
  try {
    return await CreatureModel.findAll({
      include: [
        {
          model: UserModel,
          as: 'user'
        },
        {
          model: CategoryModel,
          as: 'category'
        }
      ]
    })
  } catch (error) {
    throw error
  }
}
export const getAllCreaturesByCategories = async (categories) => {
  try {
    let query

    if (categories) {
      query = `SELECT creature.*,
          to_json (usert) "user",
          to_json (category) "category"
        FROM creatures creature
        INNER JOIN users usert ON
          usert.username = creature.username
        INNER JOIN categories category ON
          creature."categoryId" = category.id AND
          category.id = ANY('{${categories}}'::int[]) AND
          (SELECT count(*) from "creatureVersions" WHERE "creatureVersions"."creatureId" = creature.id) > 0;`
    } else {
      query = `SELECT creature.*,
          to_json (usert) "user",
          to_json (category) "category"
        FROM creatures creature
        INNER JOIN users usert ON
          usert.username = creature.username
        INNER JOIN categories category ON
          creature."categoryId" = category.id AND
          (SELECT count(*) from "creatureVersions" WHERE "creatureVersions"."creatureId" = creature.id) > 0;`
    }

    const [data] = await db.query(query)
    return data
  } catch (error) {
    throw error
  }
}

export const getCreatureById = async (id) => {
  try {
    return await CreatureModel.findByPk(id, {
      include: [
        {
          model: UserModel,
          as: 'user'
        },
        {
          model: CategoryModel,
          as: 'category'
        }
      ]
    })
  } catch (error) {
    throw error
  }
}

export const getCreatureByUsername = async (username) => {
  try {
    return await CreatureModel.findOne({
      include: [
        {
          model: UserModel,
          as: 'user'
        },
        {
          model: CategoryModel,
          as: 'category'
        }
      ],
      where: {
        username
      }
    })
  } catch (error) {
    throw error
  }
}

export const addCreature = async (creature) => {
  try {
    return await CreatureModel.create(creature)
  } catch (error) {
    throw error
  }
}

export const updateCreatureById = async (id, creature) => {
  try {
    const { description, avatar, life, lat, lgn, categoryId } = creature
    const Creature = await CreatureModel.findByPk(id, {
      include: [
        {
          model: UserModel,
          as: 'user'
        },
        {
          model: CategoryModel,
          as: 'category'
        }
      ]
    })
    return Creature.update({ description, avatar, life, lat, lgn, categoryId })
  } catch (error) {
    throw error
  }
}

export const deleteCascadeCreatureByUsername = async (username) => {
  try {
    const creature = await CreatureModel.findOne({
      where: {
        username
      }
    })

    await CreatureVersionModel.update(
      { isActive: false },
      { where: { creatureId: creature.id } }
    )

    await ClassificationReportModel.update(
      { isActive: false },
      { where: { creatureId: creature.id } }
    )

    await FavoriteModel.destroy({ where: { creatureId: creature.id } })

    await db.query(
      `UPDATE misions as mision SET "isActive"=false FROM "creatureVersions" as creature WHERE mision."creatureVersionId"=creature.id AND creature."creatureId" = ${creature.id}`
    )

    await db.query(
      `UPDATE users SET "isActive"=false FROM wizards as wizard WHERE users.username = wizard.username AND wizard."creatureId" = ${creature.id}`
    )

    await db.query(
      `DELETE FROM reports USING "creatureVersions" as creature WHERE reports."creatureVersionId"=creature.id AND creature."creatureId" = ${creature.id} `
    )

    await db.query(
      `DELETE FROM tasks USING wizards as wizard WHERE tasks."wizardId" = wizard.id AND wizard."creatureId" = ${creature.id}`
    )

    return creature
  } catch (error) {
    throw error
  }
}

export const countAllCreatures = async () => {
  try {
    return await UserModel.count({
      where: {
        type: 'C'
      }
    })
  } catch (error) {
    throw error
  }
}

import bcrypt from 'bcryptjs'

import { addUser } from '@components/user/dao'
import { message } from '@config/message'
import { handleError, handleResponse } from '@middleware/errorHandlers'
import {
  addCreature,
  getAllCreatures,
  getAllCreaturesByCategories,
  getCreatureById,
  getCreatureByUsername,
  updateCreatureById
} from './dao'
import {
  creatureResource,
  creatureResourceWithoutUser,
  creaturesResource
} from './dto'
import { deleteGCSFile } from '@middleware/uploadGCS'

// /v1/creatures
export const listCreatures = async (req, res) => {
  try {
    const { categories, type } = req.token

    let data

    if (categories) data = await getAllCreaturesByCategories(categories)
    else if (type === 'CO') data = await getAllCreaturesByCategories(null)
    else data = await getAllCreatures()

    const response = await creaturesResource(data)

    handleResponse(res, 200, message.success_long, response)
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/creatures/{id}:
export const findCreatureById = async (req, res) => {
  try {
    const data = await getCreatureById(req.params.id)
    const response = await creatureResource(data)

    handleResponse(res, 200, message.success_long, response)
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/creatures/user/{username}:
export const findCreatureByUsername = async (req, res) => {
  try {
    const data = await getCreatureByUsername(req.params.username)
    const response = await creatureResource(data)

    handleResponse(res, 200, message.success_long, response)
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/creatures
export const registerCreature = async (req, res) => {
  try {
    const creature = req.body

    const salt = await bcrypt.genSalt(10)
    const passwordEncrip = await bcrypt.hash(creature.password, salt)
    creature.password = passwordEncrip

    creature.type = 'C'

    await addUser(creature)

    if (req.files.length > 0) {
      creature.avatar = req.files[0].url
    }

    const data = await addCreature(creature)

    handleResponse(
      res,
      200,
      message.create_success,
      creatureResourceWithoutUser(data)
    )
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/creatures/{id}:
export const updateCreature = async (req, res) => {
  try {
    const id = req.params.id
    const creature = req.body

    if (req.files.length > 0) {
      creature.avatar = req.files[0].url
      const creatur = await getCreatureById(id)

      if (creatur?.avatar !== creature.avatar) {
        await deleteGCSFile([creatur.avatar])
      }
    }

    const data = await updateCreatureById(id, creature)
    const response = await creatureResource(data)

    handleResponse(res, 200, message.update, response)
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/creatures/avatar/{id}:
export const updateAvatarCreature = async (req, res) => {
  try {
    const id = req.params.id
    const { avatar } = req.body

    const data = await updateCreatureById(id, { avatar })
    const response = await creatureResource(data)

    handleResponse(res, 200, message.update, response)
  } catch (error) {
    handleError(error, res)
  }
}

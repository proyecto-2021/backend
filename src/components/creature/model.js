import { DataTypes } from 'sequelize'
import { v4 as uuidv4 } from 'uuid'

export const dataCreature = {
  // id: {
  //   type: DataTypes.UUID,
  //   primaryKey: true,
  //   defaultValue: uuidv4()
  // },
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  username: {
    type: DataTypes.STRING,
    allowNull: false,
    unique: true
  },
  life: {
    type: DataTypes.REAL,
    defaultValue: 0
  },
  description: {
    type: DataTypes.STRING
  },
  categoryId: {
    type: DataTypes.INTEGER
  },
  lat: {
    type: DataTypes.REAL,
    defaultValue: 0
  },
  lgn: {
    type: DataTypes.REAL,
    defaultValue: 0
  },
  avatar: {
    type: DataTypes.STRING
  }
}

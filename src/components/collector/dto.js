import { getAllApprovedReportsByCollectorId } from '@components/report/dao'
import { userNoPasswordResource } from '@components/user/dto'

export const collectorResource = async (resource) => ({
  id: resource.id,
  avatar: resource.avatar,
  score: resource.score,
  stars: resource.stars,
  interest: resource.interest?.split(',') || null,
  approvedReports:
    (await getAllApprovedReportsByCollectorId(resource.id)).count || 0,
  user: userNoPasswordResource(resource.user),
  lat: resource.lat,
  lgn: resource.lgn
})

export const collectorResourceAlternative = (resource) => ({
  id: resource.id,
  avatar: resource.avatar,
  score: resource.score,
  stars: resource.stars,
  interest: resource.interest?.split(',') || null,
  username: resource.username,
  lat: resource.lat,
  lgn: resource.lgn
})

export const collectorsResource = async (resources) =>
  await Promise.all(
    resources.map(async (resource) => await collectorResource(resource))
  )

import bcrypt from 'bcryptjs'
import { addUser } from '@components/user/dao'
import { message } from '@config/message'
import { handleError, handleResponse } from '@middleware/errorHandlers'
import {
  addCollector,
  getAllCollectors,
  getCollectorById,
  getCollectorByUsername,
  updateCollectorById
} from './dao'
import {
  collectorResource,
  collectorResourceAlternative,
  collectorsResource
} from './dto'
import { deleteGCSFile } from '@middleware/uploadGCS'

// /v1/collectors
export const listCollectors = async (req, res) => {
  try {
    const data = await getAllCollectors()
    const response = await collectorsResource(data)

    handleResponse(res, 200, message.success_long, response)
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/collectors/{id}:
export const findCollectorById = async (req, res) => {
  try {
    const data = await getCollectorById(req.params.id)
    const response = await collectorResource(data)

    handleResponse(res, 200, message.success_long, response)
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/collectors/user/{username}:
export const findCollectorByUsername = async (req, res) => {
  try {
    const data = await getCollectorByUsername(req.params.username)
    const response = await collectorResource(data)

    handleResponse(res, 200, message.success_long, response)
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/collectors
export const registerCollector = async (req, res) => {
  try {
    const collector = req.body

    if (collector?.interest) {
      collector.interest = collector.interest.toString()
    }

    const salt = await bcrypt.genSalt(10)
    const passwordEncrip = await bcrypt.hash(collector.password, salt)
    collector.password = passwordEncrip

    await addUser(collector)

    if (req.files.length > 0) {
      collector.avatar = req.files[0].url
    }

    const data = await addCollector(collector)

    handleResponse(
      res,
      200,
      message.create_success,
      collectorResourceAlternative(data)
    )
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/collectors/{id}:
export const updateCollector = async (req, res) => {
  try {
    const id = req.params.id
    const collector = req.body

    if (req.files.length > 0) {
      collector.avatar = req.files[0].url
      const collect = await getCollectorById(id)

      if (collect?.avatar !== collector.avatar) {
        await deleteGCSFile([collect.avatar])
      }
    }

    if (collector?.interest) {
      collector.interest = collector.interest.toString()
    }

    const data = await updateCollectorById(id, collector)

    handleResponse(res, 200, message.update, collectorResource(data))
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/collectors/avatar/{id}:
export const updateAvatarCollector = async (req, res) => {
  try {
    const id = req.params.id
    const { avatar } = req.body

    const data = await updateCollectorById(id, { avatar })

    handleResponse(res, 200, message.update, collectorResource(data))
  } catch (error) {
    handleError(error, res)
  }
}

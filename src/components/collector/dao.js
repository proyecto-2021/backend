import { CollectorModel, ReportModel, UserModel } from '@services/database'

export const getAllCollectors = async () => {
  try {
    return await CollectorModel.findAll({
      include: {
        model: UserModel,
        as: 'user'
      },
      order: [['score', 'DESC']]
    })
  } catch (error) {
    throw error
  }
}

export const getCollectorById = async (id) => {
  try {
    return await CollectorModel.findByPk(id, {
      include: {
        model: UserModel,
        as: 'user'
      }
    })
  } catch (error) {
    throw error
  }
}

export const getCollectorByUsername = async (username) => {
  try {
    return await CollectorModel.findOne({
      include: {
        model: UserModel,
        as: 'user'
      },
      where: {
        username: String(username).toLowerCase()
      }
    })
  } catch (error) {
    throw error
  }
}

export const addCollector = async (collector) => {
  try {
    return await CollectorModel.create(collector)
  } catch (error) {
    throw error
  }
}

export const updateCollectorById = async (id, { avatar, score, stars }) => {
  try {
    const data = { avatar, score, stars }
    const Collector = await CollectorModel.findByPk(id, {
      include: {
        model: UserModel,
        as: 'user'
      }
    })
    return Collector.update(data)
  } catch (error) {
    throw error
  }
}

export const deleteCasacadeCollectorByUsername = async (username) => {
  try {
    const collector = await CollectorModel.findOne({ username })

    await ReportModel.destroy({ where: { collectorId: collector.id } })

    return collector
  } catch (error) {
    throw error
  }
}

import { DataTypes } from 'sequelize'
import { v4 as uuidv4 } from 'uuid'

export const dataCollector = {
  // id: {
  //   type: DataTypes.UUID,
  //   primaryKey: true,
  //   defaultValue: uuidv4()
  // },
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  username: {
    type: DataTypes.STRING,
    allowNull: false,
    unique: true
  },
  avatar: {
    type: DataTypes.STRING,
    defaultValue: 'https://i.ibb.co/jJVj3Yj/Fraile.png'
  },
  interest: {
    type: DataTypes.STRING
  },
  score: {
    type: DataTypes.REAL,
    defaultValue: 0
  },
  stars: {
    type: DataTypes.INTEGER,
    defaultValue: 0
  },
  lat: {
    type: DataTypes.REAL,
    defaultValue: 0
  },
  lgn: {
    type: DataTypes.REAL,
    defaultValue: 0
  }
}

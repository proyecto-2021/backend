import { check, validationResult } from 'express-validator'
import { message } from '@config/message'
import { handleResponse } from '@middleware/errorHandlers'

import { getCollectorById, getCollectorByUsername } from '../dao'

const message_request = 'es requerido'

export const verifyUserInCollector = async (req, res, next) => {
  const { id, type, roleId } = req.token
  const { method } = req

  if (type === 'A') {
    next()
  } else if (type === 'CO') {
    if (method === 'GET') {
      next()
    } else {
      if (req.params.id) {
        const collector = await getCollectorById(req.params.id)

        if (collector?.id === id) next()
        else handleResponse(res, 403, message.forbidden_long)
      } else handleResponse(res, 403, message.forbidden_long)
    }
  } else if (roleId == 1 && method === 'GET') {
    next()
  } else {
    handleResponse(res, 403, message.forbidden_long)
  }
}

export const collectorRequest = async (req, res, next) => {
  await check('username', `username ${message_request}`).notEmpty().run(req)

  const request = validationResult(req)
  if (!request.isEmpty()) {
    return res.status(422).json(request)
  } else {
    next()
  }
}

export const collectorExistRequest = async (req, res, next) => {
  const collector = await getCollectorById(req.params.id)

  if (!collector) {
    handleResponse(res, 400, message.not_found_long)
  } else {
    next()
  }
}

export const usernameCollectorExistRequest = async (req, res, next) => {
  const collector = await getCollectorByUsername(req.params.username)

  if (!collector) {
    handleResponse(res, 400, message.not_found_long)
  } else {
    next()
  }
}

import { userValidators } from '@components/auth/routes'
import { sendUploadsToGCS } from '@middleware/uploadGCS'
import { uploadInMemoryImages } from '@middleware/uploadImages'
import { Router } from 'express'
import {
  findCollectorByUsername,
  findCollectorById,
  listCollectors,
  registerCollector,
  updateCollector,
  updateAvatarCollector
} from './controller'
import {
  collectorExistRequest,
  collectorRequest,
  usernameCollectorExistRequest,
  verifyUserInCollector
} from './middleware/validators'

const router = Router()

router.get('/', verifyUserInCollector, listCollectors)
router.get(
  '/:id',
  [verifyUserInCollector, collectorExistRequest],
  findCollectorById
)
router.get(
  '/user/:username',
  [verifyUserInCollector, usernameCollectorExistRequest],
  findCollectorByUsername
)
router.post(
  '/',
  uploadInMemoryImages({ type: 'any' }),
  [verifyUserInCollector, userValidators, collectorRequest],
  sendUploadsToGCS(),
  registerCollector
)
router.put(
  '/:id',
  uploadInMemoryImages({ type: 'any' }),
  [verifyUserInCollector, collectorExistRequest],
  sendUploadsToGCS(),
  updateCollector
)
router.put(
  '/avatar/:id',
  [verifyUserInCollector, collectorExistRequest],
  updateAvatarCollector
)

export const collectorRoutes = router

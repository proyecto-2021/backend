import { countAllCreatures } from '@components/creature/dao'
import {
  countAllErrors,
  countAllImprovements,
  countErrorsByCreature,
  countErrorsByWizard,
  countImprovementsByCreature,
  countImprovementsByWizard,
  getCountReportsDelayed,
  getCountReportsDelayedWithCreatureId,
  getCountReportsDelayedWithWizardId
} from '@components/report/dao'
import {
  getWizardsPerRole,
  getWizardsPerRoleWithCreatureId
} from '@components/role/dao'
import {
  getWizardByRoleId1,
  getWizardByRoleId1AndCreatureId
} from '@components/wizard/dao'
import { message } from '@config/message'
import { handleError, handleResponse } from '@middleware/errorHandlers'

export const dashboard = async (req, res) => {
  try {
    const { creatureId, roleId, type, id } = req.token

    let response
    let delayeds
    let roles
    let wizards
    let improvements
    let errors

    if (type === 'A') {
      const creatures = await countAllCreatures()
      delayeds = await getCountReportsDelayed()
      improvements = await countAllImprovements()
      errors = await countAllErrors()
      wizards = await getWizardByRoleId1()
      roles = await getWizardsPerRole()

      response = {
        creatures,
        delayeds,
        improvements,
        errors,
        wizards,
        roles
      }
    } else if (type === 'C') {
      delayeds = await getCountReportsDelayedWithCreatureId(id)
      improvements = await countImprovementsByCreature(id)
      errors = await countErrorsByCreature(id)
      wizards = await getWizardByRoleId1AndCreatureId(id)
      roles = await getWizardsPerRoleWithCreatureId(id)

      response = {
        delayeds,
        improvements,
        errors,
        wizards,
        roles
      }
    } else if (roleId === 1) {
      delayeds = await getCountReportsDelayedWithCreatureId(creatureId)
      improvements = await countImprovementsByCreature(creatureId)
      errors = await countErrorsByCreature(creatureId)
      roles = await getWizardsPerRoleWithCreatureId(creatureId)

      response = {
        delayeds,
        improvements,
        errors,
        roles
      }
    } else if (type === 'W') {
      delayeds = await getCountReportsDelayedWithWizardId(id)
      improvements = await countImprovementsByWizard(id)
      errors = await countErrorsByWizard(id)

      response = {
        delayeds,
        improvements,
        errors
      }
    }

    handleResponse(res, 200, message.accepted, response)
  } catch (error) {
    handleError(error, res)
  }
}

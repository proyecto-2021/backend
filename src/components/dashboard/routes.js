import { Router } from 'express'
import { dashboard } from './controller'

const router = Router()

router.get('/', dashboard)

export const dashboardRoutes = router

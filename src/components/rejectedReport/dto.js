export const rejectedReportResource = (resource) => ({
  id: resource.id,
  description: resource.description,
  report: resource.report,
  wizard: resource.wizard
})

export const rejectedReportResourceAlternative = (resource) => ({
  id: resource.id,
  reportId: resource.reportId,
  wizardId: resource.wizardId,
  description: resource.description
})

export const rejectedReportsResource = (resources) =>
  resources.map((resource) => rejectedReportResource(resource))

import { Router } from 'express'
import {
  findRejectedReportById,
  listRejectedReports,
  registerRejectedReport,
  updateRejectedReport,
  findRejectedReportsByWizard
} from './controller'
import {
  rejectedReportExistRequest,
  rejectedReportRequest,
  rejectedReportUpdateRequest,
  wizardRejectedReportsExist,
  authAdminOrSupremeWizard,
  verifyUserInRejectedReport
} from './middleware/validators'

const router = Router()

router.get('/', listRejectedReports)
router.get(
  '/:id',
  [rejectedReportExistRequest, verifyUserInRejectedReport],
  findRejectedReportById
)
router.get(
  '/wizard/:wizardId',
  [verifyUserInRejectedReport, wizardRejectedReportsExist],
  findRejectedReportsByWizard
)
router.post(
  '/',
  [verifyUserInRejectedReport, rejectedReportRequest],
  registerRejectedReport
)
router.put(
  '/:id',
  [
    rejectedReportUpdateRequest,
    rejectedReportExistRequest,
    verifyUserInRejectedReport
  ],
  updateRejectedReport
)

export const rejectedReportsRouter = router

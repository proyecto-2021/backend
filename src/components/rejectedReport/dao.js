import {
  RejectedReportModel,
  ReportModel,
  WizardModel
} from '@services/database'

export const getAllRejectedReports = async () => {
  try {
    return await RejectedReportModel.findAll({
      include: [
        {
          model: ReportModel,
          as: 'report'
        },
        {
          model: WizardModel,
          as: 'wizard'
        }
      ]
    })
  } catch (error) {
    throw error
  }
}
export const getAllRejectedReportsByWizar = async (wizardId) => {
  try {
    return await RejectedReportModel.findAll({
      include: [
        {
          model: ReportModel,
          as: 'report'
        },
        {
          model: WizardModel,
          as: 'wizard'
        }
      ],
      where: { wizardId }
    })
  } catch (error) {
    throw error
  }
}

export const getRejectedReportById = async (id) => {
  try {
    return await RejectedReportModel.findByPk(id, {
      include: [
        {
          model: ReportModel,
          as: 'report'
        },
        {
          model: WizardModel,
          as: 'wizard'
        }
      ]
    })
  } catch (error) {
    throw error
  }
}

export const getRejectedReportsByWizard = async (wizardId) => {
  try {
    return await RejectedReportModel.findAll({
      where: {
        wizardId
      },
      include: [
        {
          model: ReportModel,
          as: 'report'
        },
        {
          model: WizardModel,
          as: 'wizard'
        }
      ]
    })
  } catch (error) {
    throw error
  }
}

export const addRejectedReport = async (rejectedReport) => {
  try {
    return await RejectedReportModel.create(rejectedReport)
  } catch (error) {
    throw error
  }
}

export const updateRejectedReportById = async (id, rejectedReport) => {
  try {
    const RejectedReport = await RejectedReportModel.findByPk(id)

    return RejectedReport.update(rejectedReport)
  } catch (error) {
    throw error
  }
}

export const updateRejectedReportDescriptionById = async (
  id,
  rejectedReport
) => {
  try {
    let RejectedReport = await RejectedReportModel.findByPk(id)
    return RejectedReport.update({ description: rejectedReport.description })
  } catch (error) {
    throw error
  }
}

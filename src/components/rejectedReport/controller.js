import { updateStatus } from '@components/report/dao'
import { message } from '@config/message'
import { handleError, handleResponse } from '@middleware/errorHandlers'
import { getReportById } from '@components/report/dao'
import { addNotification } from '@components/notification/dao'
import {
  addRejectedReport,
  getAllRejectedReports,
  getRejectedReportsByWizard,
  getRejectedReportById,
  updateRejectedReportById,
  updateRejectedReportDescriptionById,
  getAllRejectedReportsByWizar
} from './dao'
import {
  rejectedReportResource,
  rejectedReportResourceAlternative,
  rejectedReportsResource
} from './dto'

// /v1/reports/rejected
export const listRejectedReports = async (req, res) => {
  try {
    const { type, id } = req.token
    let data

    type === 'W'
      ? (data = await getAllRejectedReportsByWizar(id))
      : (data = await getAllRejectedReports())

    handleResponse(
      res,
      200,
      message.success_long,
      rejectedReportsResource(data)
    )
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/reports/rejected/{id}:
export const findRejectedReportById = async (req, res) => {
  try {
    const data = await getRejectedReportById(req.params.id)

    handleResponse(res, 200, message.success_long, rejectedReportResource(data))
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/reports/rejected/wizard/{wizardId}:
export const findRejectedReportsByWizard = async (req, res) => {
  try {
    const data = await getRejectedReportsByWizard(req.params.wizardId)

    handleResponse(
      res,
      200,
      message.success_long,
      rejectedReportsResource(data)
    )
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/reports/rejected
export const registerRejectedReport = async (req, res) => {
  try {
    const { type, id } = req.token
    const rejectedReport = req.body

    if (type === 'W') rejectedReport.wizardId = id

    //busco el reporte para obtener el id del collector
    const report = await getReportById(rejectedReport.reportId)
    const { collector } = report
    const collectorModel = collector.dataValues

    const notification = {
      collectorId: collectorModel.id,
      name: 'Estatus del Reporte',
      description: `Se ha cambiado el estado de tu reporte a: rechazado. Razón: ${rejectedReport?.description}`
    }
    await addNotification(notification)

    await updateStatus(rejectedReport.reportId, { status: 'R' })

    const data = await addRejectedReport(rejectedReport)

    handleResponse(
      res,
      200,
      message.create_success,
      rejectedReportResourceAlternative(data)
    )
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/reports/rejected/{id}:
export const updateRejectedReport = async (req, res) => {
  try {
    const id = req.params.id
    const rejectedReport = req.body

    const data = await updateRejectedReportDescriptionById(id, rejectedReport)

    handleResponse(res, 200, message.update, rejectedReportResource(data))
  } catch (error) {
    handleError(error, res)
  }
}

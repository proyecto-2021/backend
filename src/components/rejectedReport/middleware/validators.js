import jwt from 'jsonwebtoken'
import { check, validationResult } from 'express-validator'
import { message } from '@config/message'
import { handleResponse, handleError } from '@middleware/errorHandlers'

import { getRejectedReportById, getRejectedReportsByWizard } from '../dao'

import { getWizardById } from '../../wizard/dao'
import { getReportById } from '@components/report/dao'

const message_request = 'es requerido'

export const verifyUserInRejectedReport = async (req, res, next) => {
  const { id, type, creatureId, roleId } = req.token
  const { method } = req

  if (type === 'A') {
    next()
  } else if (roleId === 1) {
    if (!req.params.id && !req.params.wizardId) {
      next()
    } else if (req.params.wizardId) {
      const wizard = await getWizardById(req.params.wizardId)

      if (wizard?.creatureId === creatureId) {
        next()
      } else {
        handleResponse(res, 403, message.forbidden_long)
      }
    } else if (req.params.id) {
      const report = await getReportById(req.rejected.reportId)
      const creature = report.creatureVersion

      if (creature?.creatureId === creatureId) {
        next()
      } else {
        handleResponse(res, 403, message.forbidden_long)
      }
    } else {
      handleResponse(res, 403, message.forbidden_long)
    }
  } else if (type === 'CO') {
    if (method === 'GET') {
      if (req.params.id) {
        const report = await getReportById(req.rejected.reportId)

        if (report?.collectorId === id) {
          next()
        } else {
          handleResponse(res, 403, message.forbidden_long)
        }
      } else handleResponse(res, 403, message.forbidden_long)
    } else handleResponse(res, 403, message.forbidden_long)
  } else {
    handleResponse(res, 403, message.forbidden_long)
  }
}

export const rejectedReportRequest = async (req, res, next) => {
  await check('reportId', `reportId ${message_request}`).notEmpty().run(req)
  await check('description', `description ${message_request}`)
    .notEmpty()
    .run(req)

  const request = validationResult(req)
  if (!request.isEmpty()) {
    return res.status(422).json(request)
  } else {
    next()
  }
}

export const rejectedReportUpdateRequest = async (req, res, next) => {
  await check('description', `description ${message_request}`)
    .notEmpty()
    .run(req)

  const request = validationResult(req)
  if (!request.isEmpty()) {
    return res.status(422).json(request)
  } else {
    next()
  }
}

export const rejectedReportExistRequest = async (req, res, next) => {
  const rejectedReport = await getRejectedReportById(req.params.id)

  if (!rejectedReport) {
    handleResponse(res, 400, message.not_found_long)
  } else {
    req.rejected = rejectedReport
    next()
  }
}

export const wizardRejectedReportsExist = async (req, res, next) => {
  const wizard = await getWizardById(req.params.wizardId)
  const creature = await getRejectedReportsByWizard(req.params.wizardId)

  if (!wizard && !creature) {
    handleResponse(res, 400, message.not_found_long)
  } else {
    next()
  }
}

export const authAdminOrSupremeWizard = (req, res, next) => {
  try {
    const bearerHeader = req.headers.authorization
    if (!req.headers.authorization) {
      return handleResponse(res, 400, 'access denied')
    }

    const bearer = bearerHeader.split(' ')
    const bearerToken = bearer[1]

    jwt.verify(bearerToken, 'r=2%P%25TDMNBaC6', (err, data) => {
      if (err) {
        res.status(403).json({ mensaje: 'invalid token' })
      } else {
        const token = jwt.decode(bearerToken)

        if (token.type === 'A' || (token.type === 'W' && token.roleId === 1)) {
          req.token = token
          next()
        } else {
          res.status(401).json({ mensaje: 'no tienes autorización' })
        }
      }
    })
  } catch (error) {
    handleError(error, res)
  }
}

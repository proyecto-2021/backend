export const roleResource = (resource) => ({
  id: resource.id,
  name: resource.name,
  description: resource.description,
  avatar: resource.avatar,
  isActive: resource.isActive
})

export const roleList = (resources) =>
  resources.map((resource) => roleResource(resource))
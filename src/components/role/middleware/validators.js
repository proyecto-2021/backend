import { check, validationResult } from 'express-validator'
import { message } from '@config/message'
import { handleResponse } from '@middleware/errorHandlers'
import { getRoleById, getRoleByName } from '../dao'

const message_request = 'es requerido'

export const verifyUserInRole = async (req, res, next) => {
  const { type } = req.token
  const { method } = req

  if (type === 'A') {
    next()
  } else if (['C', 'W'].includes(type) && method === 'GET') {
    next()
  } else {
    handleResponse(res, 403, message.forbidden_long)
  }
}

export const roleRequest = async (req, res, next) => {
  await check('name', `name ${message_request}`).notEmpty().run(req)

  const request = validationResult(req)
  if (!request.isEmpty()) {
    return res.status(422).json(request)
  } else {
    next()
  }
}

export const roleExistRequest = async (req, res, next) => {
  const role = await getRoleById(req.params.id)

  console.log('exist', !role)

  if (!role) {
    handleResponse(res, 400, message.not_found_long)
  } else {
    next()
  }
}

export const roleNameExistRequest = async (req, res, next) => {
  const role = await getRoleByName(req.body.name)

  const nameAlreadyInUser = role?.dataValues.id != req.params.id

  if (role && nameAlreadyInUser) {
    handleResponse(res, 400, message.duplicate_data)
  } else {
    next()
  }
}

export const roleNameNotExistRequest = async (req, res, next) => {
  const role = await getRoleByName(req.body.name)

  if (role) {
    handleResponse(res, 400, message.duplicate_data)
  } else {
    next()
  }
}

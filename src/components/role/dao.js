/* eslint-disable no-useless-catch */
import { db, RoleModel, WizardModel } from '@services/database'

export const getAllRoles = async () => {
  try {
    return await RoleModel.findAll()
  } catch (error) {
    throw error
  }
}

export const getAllRolesActives = async () => {
  try {
    return await RoleModel.findAll({ where: { isActive: true } })
  } catch (error) {
    throw error
  }
}

export const getRoleById = async (id) => {
  try {
    return await RoleModel.findByPk(id)
  } catch (error) {
    throw error
  }
}

export const getRoleByName = async (name) => {
  try {
    return await RoleModel.findOne({
      where: {
        name
      }
    })
  } catch (error) {
    throw error
  }
}

export const getWizardsPerRole = async () => {
  try {
    return await RoleModel.findAll({
      attributes: [
        'id',
        'name',
        [
          db.Sequelize.fn('COUNT', db.Sequelize.col('wizards.id')),
          'wizardCount'
        ]
      ],
      include: [
        {
          model: WizardModel,
          as: 'wizards',
          attributes: []
        }
      ],
      group: ['role.id'],
      order: ['id']
    })
  } catch (error) {
    throw error
  }
}

export const getWizardsPerRoleWithCreatureId = async (creatureId) => {
  try {
    return await RoleModel.findAll({
      attributes: [
        'id',
        'name',
        [
          db.Sequelize.fn('COUNT', db.Sequelize.col('wizards.id')),
          'wizardCount'
        ]
      ],
      include: [
        {
          model: WizardModel,
          as: 'wizards',
          where: {
            creatureId
          },
          attributes: []
        }
      ],
      group: ['role.id'],
      order: ['id']
    })
  } catch (error) {
    throw error
  }
}

export const addRole = async (role) => {
  try {
    return await RoleModel.create(role)
  } catch (error) {
    throw error
  }
}

export const updateRoleById = async (id, role) => {
  try {
    const Role = await RoleModel.findByPk(id)
    return Role.update(role)
  } catch (error) {
    throw error
  }
}

export const deleteRoleById = async (id) => {
  try {
    const role = await RoleModel.findByPk(id)
    return role.update({ isActive: false })
  } catch (error) {
    throw error
  }
}

export const activeRoleById = async (id) => {
  try {
    const role = await RoleModel.findByPk(id)
    return role.update({ isActive: true })
  } catch (error) {
    throw error
  }
}

import { sendUploadsToGCS } from '@middleware/uploadGCS'
import { uploadInMemoryImages } from '@middleware/uploadImages'
import { Router } from 'express'
import {
  activeRole,
  deleteRole,
  findRoleById,
  listRoles,
  listRolesActives,
  registerRole,
  updateRole
} from './controller'
import {
  roleExistRequest,
  roleNameExistRequest,
  roleNameNotExistRequest,
  roleRequest
} from './middleware/validators'

const router = Router()

router.get('/', listRolesActives)
router.get('/all/', listRoles)
router.get('/:id', [roleExistRequest], findRoleById)
router.post(
  '/',
  uploadInMemoryImages({ type: 'any' }),
  [roleRequest, roleNameNotExistRequest],
  sendUploadsToGCS(),
  registerRole
)
router.put(
  '/:id',
  uploadInMemoryImages({ type: 'any' }),
  [roleExistRequest, roleRequest, roleNameExistRequest],
  sendUploadsToGCS(),
  updateRole
)
router.delete('/:id', [roleExistRequest], deleteRole)
router.patch('/:id', [roleExistRequest], activeRole)

export const roleRoutes = router

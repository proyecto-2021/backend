import { message } from '@config/message'
import { handleError, handleResponse } from '@middleware/errorHandlers'
import { deleteGCSFile } from '@middleware/uploadGCS'
import {
  activeRoleById,
  addRole,
  deleteRoleById,
  getAllRoles,
  getAllRolesActives,
  getRoleById,
  updateRoleById
} from './dao'
import { roleResource, roleList } from './dto'

export const listRoles = async (req, res) => {
  try {
    const data = await getAllRoles()

    handleResponse(res, 200, message.success_long, roleList(data))
  } catch (error) {
    handleError(error, res)
  }
}

export const listRolesActives = async (req, res) => {
  try {
    const data = await getAllRolesActives()

    handleResponse(res, 200, message.success_long, roleList(data))
  } catch (error) {
    handleError(error, res)
  }
}

export const findRoleById = async (req, res) => {
  try {
    const data = await getRoleById(req.params.id)

    handleResponse(res, 200, message.success_long, roleResource(data))
  } catch (error) {
    handleError(error, res)
  }
}

export const registerRole = async (req, res) => {
  try {
    const role = req.body

    if (req.files.length > 0) {
      role.avatar = req.files[0].url
    }

    const data = await addRole(role)

    handleResponse(res, 200, message.create_success, roleResource(data))
  } catch (error) {
    handleError(error, res)
  }
}

export const updateRole = async (req, res) => {
  try {
    const id = req.params.id
    const role = req.body

    if (req.files.length > 0) {
      role.avatar = req.files[0].url
      const rol = await getRoleById(id)

      if (rol?.avatar !== role.avatar) {
        await deleteGCSFile([rol.avatar])
      }
    }

    const data = await updateRoleById(id, role)

    handleResponse(res, 200, message.update, roleResource(data))
  } catch (error) {
    handleError(error, res)
  }
}

export const deleteRole = async (req, res) => {
  try {
    const data = await deleteRoleById(req.params.id)

    handleResponse(res, 200, message.update, roleResource(data))
  } catch (error) {
    handleError(error, res)
  }
}

export const activeRole = async (req, res) => {
  try {
    const data = await activeRoleById(req.params.id)

    handleResponse(res, 200, message.update, roleResource(data))
  } catch (error) {
    handleError(error, res)
  }
}

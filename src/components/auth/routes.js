import {
  userEmailNotExistRequest,
  userNotExistRequest,
  userRequest,
  validatePassword
} from '@components/user/middleware/validators'
import { Router } from 'express'
import { login, register, restartPassword } from './controller'
import { authCredentialsRequest, loginRequest, restartPasswordRequest } from './middlewares/validator'

const router = Router()
export const userValidators = [
  userRequest,
  userNotExistRequest,
  userEmailNotExistRequest,
  validatePassword
]

router.post('/login', [loginRequest, authCredentialsRequest], login)
router.post('/register', userValidators, register)
router.post('/restartPassword', restartPasswordRequest, restartPassword)

export const authRoutes = router

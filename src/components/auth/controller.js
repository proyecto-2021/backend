import bcrypt from 'bcryptjs'
import { addCollector, getCollectorByUsername } from '@components/collector/dao'
import { getCreatureByUsername } from '@components/creature/dao'
import { addUser, getUserByEmail } from '@components/user/dao'
import { getAllWizardByUsername } from '@components/wizard/dao'
import { message } from '@config/message'
import { handleError, handleResponse } from '@middleware/errorHandlers'
import { generateJWT } from '@services/jwt'
import { nanoid } from 'nanoid'
import { sendEmail } from '@services/email'

export const login = async (req, res) => {
  try {
    let user = req.body.user.dataValues
    const type = user?.type

    switch (type) {
      case 'W':
        const wizard = await getAllWizardByUsername(user.username)
        user = wizard
        user.type = type
        break
      case 'C':
        const creature = await getCreatureByUsername(user.username)
        user = creature
        user.type = type
        break
      case 'CO':
        const collector = await getCollectorByUsername(user.username)
        user = collector
        user.type = type
        break
      default:
        break
    }

    const token = generateJWT(user)

    const data = {
      token: token,
      id: user.id || user.username,
      name:
        `${user.firstName} ${user.lastName}` ||
        `${user.user?.firstName} ${user.user?.lastName}` ||
        null,
      username: user.username,
      type: user.type || ''
    }

    handleResponse(res, 200, message.success, data)
  } catch (error) {
    handleError(error, res)
  }
}

export const register = async (req, res) => {
  try {
    const dataUser = req.body

    const salt = await bcrypt.genSalt(10)
    const passwordEncrip = await bcrypt.hash(dataUser.password, salt)
    dataUser.password = passwordEncrip

    const user = await addUser(dataUser)

    if (dataUser?.interest) {
      await addCollector({
        username: user.username,
        interest: dataUser.interest.toString()
      })
    } else {
      await addCollector({
        username: user.username
      })
    }

    handleResponse(res, 200, message.user_success)
  } catch (error) {
    handleError(error, res)
  }
}

export const restartPassword = async (req, res) => {
  try {
    const { email } = req.body

    const user = await getUserByEmail(email)

    const newPassword = nanoid(8)
    const salt = await bcrypt.genSalt(10)
    const passwordEncrip = await bcrypt.hash(newPassword, salt)
    user.password = passwordEncrip

    await user.save()

    await sendEmail(email, 'cambio contraseña', 'newpassword', {
      password: newPassword
    })

    handleResponse(
      res,
      200,
      'Se envio un mensaje al correo indicado con la nueva contraseña'
    )
  } catch (error) {
    handleError(error, res)
  }
}

import bcrypt from 'bcryptjs'
import { getUserByEmail, getUserByUsername } from '@components/user/dao'
import { handleError, handleResponse } from '@middleware/errorHandlers'
import { check, validationResult } from 'express-validator'

export const authCredentialsRequest = async (req, res, next) => {
  try {
    const findUser = await getUserByUsername(req.body.username)
    if (!findUser) {
      return handleResponse(res, 400, 'Credenciales invalidas')
    }

    if (!findUser.isActive) {
      return handleResponse(res, 400, 'Usuario invalido')
    }

    // validate password
    const isValidPassword = await bcrypt.compare(
      req.body.password,
      findUser.password
    )

    if (!isValidPassword) {
      return handleResponse(res, 400, 'Credenciales invalidas')
    }

    req.body.user = findUser
    next()
  } catch (error) {
    handleError(error, res)
  }
}

export const loginRequest = async (req, res, next) => {
  try {
    await check('username', `username es requerido`).notEmpty().run(req)
    await check('password', `password es requerido`).notEmpty().run(req)
    const request = validationResult(req)
    if (!request.isEmpty()) {
      return res.status(422).json(request)
    } else {
      next()
    }
  } catch (error) {
    handleError(error, res)
  }
}

export const restartPasswordRequest = async (req, res, next) => {
  try {
    await check('email', 'email es requerido').notEmpty().run(req)
    const request = validationResult(req)
    if (!request.isEmpty()) {
      return res.status(422).json(request)
    }

    const findUser = await getUserByEmail(req.body.email)

    if (!findUser) {
      return handleResponse(res, 404, 'correo no encontrado')
    }

    next()
  } catch (error) {
    handleError(error, res)
  }
}

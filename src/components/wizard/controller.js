import bcrypt from 'bcryptjs'

import { addUser } from '@components/user/dao'
import { message } from '@config/message'
import { handleError, handleResponse } from '@middleware/errorHandlers'
import {
  addWizard,
  deleteWizardById,
  getAllWizardByUsername,
  getAllWizards,
  getAllWizardsByCreature,
  getWizardById,
  updateWizardById
} from './dao'
import {
  wizardResource,
  wizardResourceAlternative,
  wizardsResource
} from './dto'

export const listWizards = async (req, res) => {
  try {
    const creatureId = req.creatureId
    let data

    creatureId
      ? (data = await getAllWizardsByCreature(creatureId))
      : (data = await getAllWizards())

    handleResponse(res, 200, message.success_long, wizardsResource(data))
  } catch (error) {
    handleError(error, res)
  }
}

export const findWizardById = async (req, res) => {
  try {
    const data = await getWizardById(req.params.id)

    handleResponse(res, 200, message.success_long, wizardResource(data))
  } catch (error) {
    handleError(error, res)
  }
}

export const findWizardByUsername = async (req, res) => {
  try {
    const data = await getAllWizardByUsername(req.params.username)

    handleResponse(res, 200, message.success_long, wizardResource(data))
  } catch (error) {
    handleError(error, res)
  }
}

export const registerWizard = async (req, res) => {
  try {
    const wizard = req.body
    const creatureId = req.creatureId

    const salt = await bcrypt.genSalt(10)
    const passwordEncrip = await bcrypt.hash(wizard.password, salt)
    wizard.password = passwordEncrip
    wizard.type = 'W'

    if (creatureId) wizard.creatureId = creatureId

    await addUser(wizard)
    const data = await addWizard(wizard)

    handleResponse(
      res,
      200,
      message.create_success,
      wizardResourceAlternative(data)
    )
  } catch (error) {
    handleError(error, res)
  }
}

export const updateWizard = async (req, res) => {
  try {
    const id = req.params.id
    const wizard = req.body

    const data = await updateWizardById(id, wizard)

    handleResponse(res, 200, message.update, wizardResource(data))
  } catch (error) {
    handleError(error, res)
  }
}

export const deleteWizard = async (req, res) => {
  try {
    const data = await deleteWizardById(req.params.id)

    handleResponse(res, 200, message.delete, wizardResourceAlternative(data))
  } catch (error) {
    handleError(error, res)
  }
}

import { userValidators } from '@components/auth/routes'
import { Router } from 'express'
import {
  deleteWizard,
  findWizardById,
  findWizardByUsername,
  listWizards,
  registerWizard,
  updateWizard
} from './controller'
import {
  usernameWizardExistRequest,
  verifyUserInWizard,
  wizardExistRequest,
  wizardRequest
} from './middleware/validators'

const router = Router()

router.get('/', verifyUserInWizard, listWizards)
router.get('/:id', [wizardExistRequest, verifyUserInWizard], findWizardById)
router.get(
  '/user/:username',
  [verifyUserInWizard, usernameWizardExistRequest],

  findWizardByUsername
)
router.post(
  '/',
  [userValidators, wizardRequest, verifyUserInWizard],
  registerWizard
)
router.put(
  '/:id',
  [wizardExistRequest, wizardRequest, verifyUserInWizard],
  updateWizard
)
router.delete('/:id', [wizardExistRequest, verifyUserInWizard], deleteWizard)

export const wizardRoutes = router

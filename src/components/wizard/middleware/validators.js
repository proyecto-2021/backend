import { check, validationResult } from 'express-validator'
import { message } from '@config/message'
import { handleResponse } from '@middleware/errorHandlers'
import { getCreatureById } from '@components/creature/dao'
import { getAllWizardByUsername, getWizardById } from '../dao'

const message_request = 'es requerido'

export const verifyUserInWizard = async (req, res, next) => {
  const { id, type, creatureId, roleId } = req.token
  const { method } = req

  if (type === 'A') {
    next()
  } else if (type === 'C') {
    if (!req.params.id && !req.params.username) {
      req.creatureId = id
      next()
    } else {
      if (req.params.id) {
        const wizard = await getWizardById(req.params.id)

        if (wizard?.creatureId === id) next()
        else handleResponse(res, 403, message.forbidden_long)
      } else if (req.params.username) {
        const wizard = await getAllWizardByUsername(req.params.username)

        if (wizard?.creatureId === id) next()
        else handleResponse(res, 403, message.forbidden_long)
      } else handleResponse(res, 403, message.forbidden_long)
    }
  } else if (roleId === 1) {
    if (!req.params.id && !req.params.username) {
      req.creatureId = creatureId
      next()
    } else {
      if (req.params.id) {
        const wizard = await getWizardById(req.params.id)

        if (wizard?.creatureId === creatureId) next()
        else handleResponse(res, 403, message.forbidden_long)
      } else if (req.params.username) {
        const wizard = await getAllWizardByUsername(req.params.username)

        if (wizard?.creatureId === creatureId) next()
        else handleResponse(res, 403, message.forbidden_long)
      } else handleResponse(res, 403, message.forbidden_long)
    }
  } else if (type === 'W') {
    if (req.params.id && method === 'GET') {
      const wizard = await getWizardById(req.params.id)

      if (wizard?.creatureId === creatureId) next()
      else handleResponse(res, 403, message.forbidden_long)
    } else if (req.params.username) {
      const wizard = await getAllWizardByUsername(req.params.username)

      if (wizard?.creatureId === creatureId) next()
      else handleResponse(res, 403, message.forbidden_long)
    } else if (method === 'GET') {
      req.creatureId = creatureId
      next()
    } else handleResponse(res, 403, message.forbidden_long)
  } else {
    handleResponse(res, 403, message.forbidden_long)
  }
}

export const wizardRequest = async (req, res, next) => {
  // await check('username', `username ${message_request}`).notEmpty().run(req)
  await check('roleId', `roleId ${message_request}`).notEmpty().run(req)

  const request = validationResult(req)
  if (!request.isEmpty()) {
    return res.status(422).json(request)
  } else {
    next()
  }
}

export const wizardExistRequest = async (req, res, next) => {
  const wizard = await getWizardById(req.params.id)

  if (!wizard) {
    handleResponse(res, 400, message.not_found_long)
  } else {
    next()
  }
}

export const usernameWizardExistRequest = async (req, res, next) => {
  const wizard = await getAllWizardByUsername(req.params.username)

  if (!wizard) {
    handleResponse(res, 400, message.not_found_long)
  } else {
    next()
  }
}

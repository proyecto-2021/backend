import {
  WizardModel,
  CreatureModel,
  RoleModel,
  TaskModel,
  UserModel
} from '@services/database'

export const getAllWizards = async () => {
  try {
    return await WizardModel.findAll({
      include: [
        {
          model: CreatureModel,
          as: 'creature'
        },

        {
          model: UserModel,
          as: 'user'
        },

        {
          model: RoleModel,
          as: 'role'
        }
      ],
      order: [['score', 'DESC']]
    })
  } catch (error) {
    throw error
  }
}

export const getAllWizardsByCreature = async (creatureId) => {
  try {
    return await WizardModel.findAll({
      include: [
        {
          model: CreatureModel,
          as: 'creature'
        },

        {
          model: UserModel,
          as: 'user'
        },

        {
          model: RoleModel,
          as: 'role'
        }
      ],
      where: { creatureId },
      order: [['score', 'DESC']]
    })
  } catch (error) {
    throw error
  }
}

export const getAllWizardByUsername = async (username) => {
  try {
    return await WizardModel.findOne({
      include: [
        {
          model: CreatureModel,
          as: 'creature'
        },

        {
          model: UserModel,
          as: 'user'
        },

        {
          model: RoleModel,
          as: 'role'
        }
      ],
      where: {
        username: String(username).toLowerCase()
      }
    })
  } catch (error) {
    throw error
  }
}

export const getWizardById = async (id) => {
  try {
    return await WizardModel.findByPk(id, {
      include: [
        {
          model: CreatureModel,
          as: 'creature'
        },

        {
          model: UserModel,
          as: 'user'
        },

        {
          model: RoleModel,
          as: 'role'
        }
      ]
    })
  } catch (error) {
    throw error
  }
}

export const getWizardByRoleId1 = async () => {
  try {
    return await WizardModel.count({
      where: {
        roleId: 1
      }
    })
  } catch (error) {
    throw error
  }
}

export const getWizardByRoleId1AndCreatureId = async (creatureId) => {
  try {
    return await WizardModel.count({
      where: {
        roleId: 1,
        creatureId
      }
    })
  } catch (error) {
    throw error
  }
}

export const addWizard = async (wizard) => {
  try {
    return await WizardModel.create(wizard)
  } catch (error) {
    throw error
  }
}

export const updateWizardById = async (id, wizard) => {
  try {
    const Wizard = await WizardModel.findByPk(id, {
      include: [
        {
          model: CreatureModel,
          as: 'creature'
        },

        {
          model: UserModel,
          as: 'user'
        },

        {
          model: RoleModel,
          as: 'role'
        }
      ]
    })
    return Wizard.update(wizard)
  } catch (error) {
    throw error
  }
}

export const deleteWizardById = async (id) => {
  try {
    return await WizardModel.destroy({ where: { id } })
  } catch (error) {
    throw error
  }
}

export const deleteCasacadeWizardByUsername = async (username) => {
  try {
    const wizard = await WizardModel.findOne({ username })

    if (wizard?.roleId !== 1) {
      await TaskModel.destroy({ where: { wizardId: wizard.id } })
    }

    return wizard
  } catch (error) {
    throw error
  }
}

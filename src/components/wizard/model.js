import { DataTypes } from 'sequelize'
import { v4 as uuidv4 } from 'uuid'

export const dataWizard = {
  // id: {
  //   type: DataTypes.UUID,
  //   primaryKey: true,
  //   defaultValue: uuidv4()
  // },
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  username: {
    type: DataTypes.STRING,
    allowNull: false,
    unique: true
  },
  roleId: {
    type: DataTypes.INTEGER,
    allowNull: false
  },
  creatureId: {
    type: DataTypes.INTEGER,
    allowNull: false
  },
  score: {
    type: DataTypes.REAL,
    defaultValue: 0
  }
}

import { userNoPasswordResource } from '@components/user/dto'

export const wizardResourceAlternative = (resource) => ({
  id: resource.id,
  username: resource.username,
  creatureId: resource.creatureId,
  roleId: resource.roleId,
  score: resource.score
})

export const wizardResource = (resource) => ({
  id: resource.id,
  score: resource.score,
  user: userNoPasswordResource(resource.user),
  creature: resource.creature,
  role: resource.role
})

export const wizardsResource = (resources) =>
  resources.map((resource) => wizardResource(resource))

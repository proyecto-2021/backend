export const subMissionResource = (resource) => ({
  id: resource.id,
  missionId: resource.missionId,
  name: resource.name,
  description: resource.description,
  score: resource.score,
  isActive: resource.isActive
})

export const subMissionResourceWithMission = (resource) => ({
  id: resource.id,
  mission: resource.mission,
  name: resource.name,
  description: resource.description,
  score: resource.score,
  isActive: resource.isActive
})

export const subMissionUserResourceWithMission = (resource, missions) => ({
  id: resource.id,
  name: resource.name,
  description: resource.description,
  score: resource.score,
  isActive: resource.isActive,
  completed: missions.some((item) => item.subMissionId === resource.id),
  mission: resource.mission || resource.missionId
})

export const subMissionsResource = (resources) =>
  resources.map((resource) => subMissionResourceWithMission(resource))

export const subMissionsUserResource = (resources, missions) =>
  resources.map((resource) =>
    subMissionUserResourceWithMission(resource, missions)
  )

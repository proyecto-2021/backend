import { getMissionCollectorByMissionUserId } from '@components/missionCollector/dao'
import { message } from '@config/message'
import { handleError, handleResponse } from '@middleware/errorHandlers'
import {
  addSubMission,
  getAllSubMissions,
  getAllSubMissionByMission,
  getSubMissionById,
  updateSubMissionById,
  deleteSubMissionById,
  getAllSubMissionByCreature
} from './dao'
import {
  subMissionResource,
  subMissionResourceWithMission,
  subMissionsResource,
  subMissionsUserResource
} from './dto'

// /v1/submisions
export const listSubMissions = async (req, res) => {
  try {
    const creatureId = req.creatureId

    let data = await getAllSubMissions()

    handleResponse(res, 200, message.success_long, subMissionsResource(data))
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/submissions/mission/{id}:
export const findSubMissionsByMission = async (req, res) => {
  try {
    const { id, type } = req.token
    const missionId = req.params.missionId

    const data = await getAllSubMissionByMission(missionId)
    let response

    if (type === 'CO') {
      const missions = await getMissionCollectorByMissionUserId(missionId, id)
      response = subMissionsUserResource(data, missions)
    } else response = subMissionsResource(data)

    handleResponse(res, 200, message.success_long, response)
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/submissions/creature/{id}:
export const findSubMissionsByCreature = async (req, res) => {
  try {
    const data = await getAllSubMissionByCreature(req.params.creatureId)

    handleResponse(res, 200, message.success_long, subMissionsResource(data))
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/submissions/{id}:
export const findSubMissionById = async (req, res) => {
  try {
    const data = await getSubMissionById(req.params.id)

    handleResponse(
      res,
      200,
      message.success_long,
      subMissionResourceWithMission(data)
    )
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/submissions
export const registerSubMission = async (req, res) => {
  try {
    //const creatureId = req.creatureId
    const submission = req.body
    //const creatureVersion = await getLastCreatureVersionById(creatureId)
    //mision.creatureVersionId = creatureVersion[0].dataValues.id

    const data = await addSubMission(submission)

    handleResponse(res, 200, message.create_success, subMissionResource(data))
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/submissions/{id}:
export const updateSubMission = async (req, res) => {
  try {
    const id = req.params.id
    const submission = req.body

    const data = await updateSubMissionById(id, submission)
    handleResponse(
      res,
      200,
      message.update,
      subMissionResourceWithMission(data)
    )
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/submissions/{id}:
export const deleteSubMission = async (req, res) => {
  try {
    const data = await deleteSubMissionById(req.params.id)

    handleResponse(
      res,
      200,
      message.update,
      subMissionResourceWithMission(data)
    )
  } catch (error) {
    handleError(error, res)
  }
}

import { Op } from 'sequelize'
import { MisionModel, SubMissionModel } from '@services/database'

export const getAllSubMissions = async () => {
  try {
    return await SubMissionModel.findAll({
      include: {
        model: MisionModel,
        as: 'mission'
      }
    })
  } catch (error) {
    throw error
  }
}

export const getAllSubMissionByMission = async (missionId) => {
  try {
    return await SubMissionModel.findAll({
      include: {
        model: MisionModel,
        as: 'mission'
      },
      where: {
        missionId
      }
    })
  } catch (error) {
    throw error
  }
}

export const getAllSubMissionByCreature = async (creatureId) => {
  try {
    return await SubMissionModel.findAll({
      include: {
        model: MisionModel,
        as: 'mission'
      },
      where: {
        '$mission.creatureVersionId$': creatureId
      }
    })
  } catch (error) {
    throw error
  }
}

export const getSubMissionById = async (id) => {
  try {
    return await SubMissionModel.findByPk(id, {
      include: {
        model: MisionModel,
        as: 'mission'
      }
    })
  } catch (error) {
    throw error
  }
}

export const addSubMission = async (submission) => {
  try {
    return await SubMissionModel.create(submission)
  } catch (error) {
    throw error
  }
}

export const updateSubMissionById = async (id, submission) => {
  try {
    //const { missionId, name, description, score, isActive } = submission
    const SubMission = await SubMissionModel.findByPk(id, {
      include: {
        model: MisionModel,
        as: 'mission'
      }
    })
    return SubMission.update(submission)
  } catch (error) {
    throw error
  }
}

export const deleteSubMissionById = async (id) => {
  try {
    const SubMission = await SubMissionModel.findByPk(id, {
      include: {
        model: MisionModel,
        as: 'mission'
      }
    })
    return SubMission.update({ isActive: false })
  } catch (error) {
    throw error
  }
}

import { check, validationResult } from 'express-validator'
import { message } from '@config/message'
import { handleResponse } from '@middleware/errorHandlers'

import { getSubMissionById } from '../dao'
import { getMisionById } from '@components/mision/dao'
import { getCreatureVersionById } from '@components/creatureVersion/dao'
const message_request = 'es requerido'

export const verifyUserInSubMission = async (req, res, next) => {
  const { id, type, creatureId, roleId } = req.token
  const { method } = req

  if (type === 'A') {
    next()
  } else if (type === 'C' || roleId === 1) {
    if (!req.params.id && !req.params.creatureVersionId) {
      req.creatureId = type === 'C' ? id : creatureId
      next()
    } else if (req.params.id) {
      const subMission = await getSubMissionById(req.params.id)

      const creatureV = await getCreatureVersionById(
        subMission.mission.creatureVersionId
      )

      if (
        creatureV?.creatureId === id ||
        creatureV?.creatureId === creatureId
      ) {
        next()
      } else {
        handleResponse(res, 403, message.forbidden_long)
      }
    } else if (req.params.missionId) {
      const mission = await getMisionById(req.params.missionId)

      if (
        mission?.creatureVersion?.creatureId === id ||
        mission?.creatureVersion?.creatureId === creatureId
      ) {
        next()
      } else {
        handleResponse(res, 403, message.forbidden_long)
      }
    } else {
      handleResponse(res, 403, message.forbidden_long)
    }
  } else if (type === 'W') {
    if (method === 'GET') {
      if (!req.params.id && !req.params.missionId) {
        req.creatureId = creatureId
        next()
      } else if (req.params.id) {
        const subMission = await getSubMissionById(req.params.id)

        const mision = await getMisionById(subMission?.missionId)

        const creatureV = await getCreatureVersionById(mision.creatureVersionId)

        if (creatureV?.creatureId === creatureId) next()
        else handleResponse(res, 403, message.forbidden_long)
      } else handleResponse(res, 403, message.forbidden_long)
    } else handleResponse(res, 403, message.forbidden_long)
  } else {
    if (method === 'GET') next()
    else handleResponse(res, 403, message.forbidden_long)
  }
}

export const subMissionRequest = async (req, res, next) => {
  await check('missionId', `missionId ${message_request}`).notEmpty().run(req)
  await check('name', `name ${message_request}`).notEmpty().run(req)
  await check('description', `description ${message_request}`)
    .notEmpty()
    .run(req)
  await check('score', `score ${message_request}`).notEmpty().run(req)

  const request = validationResult(req)
  if (!request.isEmpty()) {
    return res.status(422).json(request)
  } else {
    next()
  }
}

export const subMissionExistRequest = async (req, res, next) => {
  const submission = await getSubMissionById(req.params.id)

  if (!submission) {
    handleResponse(res, 400, message.not_found_long)
  } else {
    next()
  }
}

export const missionIdSubMissionExistRequest = async (req, res, next) => {
  const mission = await getMisionById(req.params.missionId)
  if (!mission) {
    handleResponse(res, 400, message.not_found_long)
  } else {
    next()
  }
}

export const creatureIdSubMissionExistRequest = async (req, res, next) => {
  const creature = await getCreatureVersionById(req.params.creatureId)
  if (!creature) {
    handleResponse(res, 400, message.not_found_long)
  } else {
    next()
  }
}

export const missionNotExistRequest = async (req, res, next) => {
  if (req.body.missionId) {
    const mission = await getMisionById(req.body.missionId)

    if (!mission) {
      handleResponse(res, 400, `Mision: ${message.not_found_long}`)
    } else {
      req.body.mission = mission.dataValues.missionId
      next()
    }
  } else {
    next()
  }
}

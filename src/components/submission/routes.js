import { Router } from 'express'
import {
  deleteSubMission,
  findSubMissionById,
  findSubMissionsByCreature,
  findSubMissionsByMission,
  listSubMissions,
  registerSubMission,
  updateSubMission
} from './controller'
import {
  missionIdSubMissionExistRequest,
  subMissionExistRequest,
  subMissionRequest,
  missionNotExistRequest,
  creatureIdSubMissionExistRequest
} from './middleware/validators'

const router = Router()

router.get('/', listSubMissions)
router.get('/:id', [subMissionExistRequest], findSubMissionById)
router.get(
  '/mission/:missionId',
  [missionIdSubMissionExistRequest],
  findSubMissionsByMission
)
router.get(
  '/creature/:creatureId',
  [creatureIdSubMissionExistRequest],
  findSubMissionsByCreature
)
router.post(
  '/',
  [subMissionRequest, missionNotExistRequest],
  registerSubMission
)
router.put('/:id', [subMissionExistRequest], updateSubMission)
router.delete('/:id', [subMissionExistRequest], deleteSubMission)

export const subMissionRoutes = router

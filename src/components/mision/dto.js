import { getMissionCollectorByMissionUser } from '@components/missionCollector/dao'

export const misionResource = (resource) => ({
  id: resource.id,
  creatureVersionId: resource.creatureVersionId,
  name: resource.name,
  score: resource.score,
  description: resource.description,
  isActive: resource.isActive
})

export const misionResourceWithCreatureVersion = (resource) => ({
  id: resource.id,
  name: resource.name,
  description: resource.description,
  score: resource.score,
  isActive: resource.isActive,
  creatureVersion: resource.creatureVersion
})

export const misionResourceWithCreatureVersionFinish = async (
  resource,
  username
) => ({
  id: resource.id,
  name: resource.name,
  description: resource.description,
  score: resource.score,
  isActive: resource.isActive,
  completed: await getMissionCollectorByMissionUser(resource.id, username),
  creatureVersion: resource.creatureVersion
})

export const misionsResource = (resources) =>
  resources.map((resource) => misionResourceWithCreatureVersion(resource))

export const misionsUserResource = async (resources, username) =>
  await Promise.all(
    resources.map(
      async (resource) =>
        await misionResourceWithCreatureVersionFinish(resource, username)
    )
  )

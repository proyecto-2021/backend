import { Router } from 'express'
import {
  activateMision,
  deleteMision,
  findMisionById,
  findMisionsByCreatureVersion,
  listMisions,
  registerMision,
  updateMision
} from './controller'
import {
  creatureVersionIdMisionExistRequest,
  misionExistRequest,
  misionRequest,
  creatureVersionNotExistRequest,
  checkLastCreatureVersion,
  verifyUserInMision
} from './middleware/validators'

const router = Router()

router.get('/', verifyUserInMision, listMisions)
router.get('/:id', [misionExistRequest, verifyUserInMision], findMisionById)
router.get(
  '/version/:creatureVersionId',
  [verifyUserInMision, creatureVersionIdMisionExistRequest],
  findMisionsByCreatureVersion
)
router.post(
  '/',
  [misionRequest, verifyUserInMision, creatureVersionNotExistRequest],
  registerMision
)
router.put(
  '/:id',
  [misionExistRequest, verifyUserInMision, misionRequest],
  updateMision
)
router.delete('/:id', [misionExistRequest, verifyUserInMision], deleteMision)
router.put(
  '/activate/:id',
  [misionExistRequest, verifyUserInMision],
  activateMision
)

export const misionRoutes = router

import { check, validationResult } from 'express-validator'
import { message } from '@config/message'
import { handleResponse } from '@middleware/errorHandlers'
import {
  getCreatureVersionById,
  getLastCreatureVersionById
} from '@components/creatureVersion/dao'
import { getMisionById } from '../dao'

const message_request = 'es requerido'

export const verifyUserInMision = async (req, res, next) => {
  const { id, type, creatureId, roleId } = req.token
  const { method } = req

  if (type === 'A') {
    next()
  } else if (type === 'C' || roleId === 1) {
    if (!req.params.id && !req.params.creatureVersionId) {
      req.creatureId = type === 'C' ? id : creatureId
      next()
    } else if (req.params.id) {
      const mision = await getMisionById(req.params.id)

      const creatureV = await getCreatureVersionById(mision.creatureVersionId)

      if (
        creatureV?.creatureId === id ||
        creatureV?.creatureId === creatureId
      ) {
        next()
      } else {
        handleResponse(res, 403, message.forbidden_long)
      }
    } else if (req.params.creatureVersionId) {
      const creatureV = await getCreatureVersionById(
        req.params.creatureVersionId
      )

      if (
        creatureV?.creatureId === id ||
        creatureV?.creatureId === creatureId
      ) {
        next()
      } else {
        handleResponse(res, 403, message.forbidden_long)
      }
    } else {
      handleResponse(res, 403, message.forbidden_long)
    }
  } else if (type === 'W') {
    if (method === 'GET') {
      if (!req.params.id && !req.params.creatureVersionId) {
        req.creatureId = creatureId
        next()
      } else if (req.params.id) {
        const mision = await getMisionById(req.params.id)

        const creatureV = await getCreatureVersionById(mision.creatureVersionId)

        if (creatureV.creatureId === creatureId) next()
        else handleResponse(res, 403, message.forbidden_long)
      } else handleResponse(res, 403, message.forbidden_long)
    } else handleResponse(res, 403, message.forbidden_long)
  } else {
    if (method === 'GET') next()
    else handleResponse(res, 403, message.forbidden_long)
  }
}

export const misionRequest = async (req, res, next) => {
  await check('description', `description ${message_request}`)
    .notEmpty()
    .run(req)
  await check('name', `name ${message_request}`).notEmpty().run(req)

  const request = validationResult(req)
  if (!request.isEmpty()) {
    return res.status(422).json(request)
  } else {
    next()
  }
}

export const misionExistRequest = async (req, res, next) => {
  const mision = await getMisionById(req.params.id)

  if (!mision) {
    handleResponse(res, 400, message.not_found_long)
  } else {
    next()
  }
}

export const creatureVersionNotExistRequest = async (req, res, next) => {
  if (req.body.creatureVersionId) {
    const creatureVersion = await getCreatureVersionById(
      req.body.creatureVersionId
    )

    if (!creatureVersion) {
      handleResponse(res, 400, message.not_found_long)
    } else {
      req.body.creature = creatureVersion.dataValues.creatureId
      next()
    }
  } else {
    next()
  }
}

export const checkLastCreatureVersion = async (req, res, next) => {
  if (req.body.creatureVersionId) {
    const creatureVersion = await getLastCreatureVersionById(req.body.creature)

    if (creatureVersion[0].dataValues.id != req.body.creatureVersionId) {
      handleResponse(res, 400, message.not_found_creature_version)
    } else {
      next()
    }
  } else {
    next()
  }
}

export const creatureVersionIdMisionExistRequest = async (req, res, next) => {
  const creatureVersion = await getCreatureVersionById(
    req.params.creatureVersionId
  )

  if (!creatureVersion) {
    handleResponse(res, 400, message.not_found_long)
  } else {
    next()
  }
}

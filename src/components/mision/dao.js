import { Op } from 'sequelize'
import {
  MisionModel,
  CreatureVersionModel,
  CreatureModel
} from '@services/database'

export const getAllMisions = async () => {
  try {
    return await MisionModel.findAll({
      include: {
        model: CreatureVersionModel,
        as: 'creatureVersion'
      }
    })
  } catch (error) {
    throw error
  }
}

export const getAllMisionByCreatureVersion = async (creatureVersionId) => {
  try {
    return await MisionModel.findAll({
      include: {
        model: CreatureVersionModel,
        as: 'creatureVersion'
      },
      where: {
        creatureVersionId
      }
    })
  } catch (error) {
    throw error
  }
}

export const getAllMisionByCreature = async (creatureId) => {
  try {
    return await MisionModel.findAll({
      include: [
        {
          model: CreatureVersionModel,
          as: 'creatureVersion'
        }
      ],
      where: { '$creatureVersion.creatureId$': creatureId }
    })
  } catch (error) {
    throw error
  }
}

export const getMisionById = async (id) => {
  try {
    return await MisionModel.findByPk(id, {
      include: {
        model: CreatureVersionModel,
        as: 'creatureVersion'
      }
    })
  } catch (error) {
    throw error
  }
}

export const addMision = async (creature) => {
  try {
    return await MisionModel.create(creature)
  } catch (error) {
    throw error
  }
}

export const updateMisionById = async (id, mision) => {
  try {
    const { name, description } = mision
    const Mision = await MisionModel.findByPk(id, {
      include: {
        model: CreatureVersionModel,
        as: 'creatureVersion'
      }
    })
    return Mision.update({ name, description })
  } catch (error) {
    throw error
  }
}

export const deleteMisionById = async (id) => {
  try {
    const Mision = await MisionModel.findByPk(id, {
      include: {
        model: CreatureVersionModel,
        as: 'creatureVersion'
      }
    })
    return Mision.update({ isActive: false })
  } catch (error) {
    throw error
  }
}

export const activateMisionById = async (id) => {
  try {
    const Mision = await MisionModel.findByPk(id, {
      include: {
        model: CreatureVersionModel,
        as: 'creatureVersion'
      }
    })
    return Mision.update({ isActive: true })
  } catch (error) {
    throw error
  }
}

export const delCascadeMisionByCreatureVersion = async (creatureVersionId) => {
  try {
    return await MisionModel.update(
      { isActive: false },
      { where: { creatureVersionId } }
    )
  } catch (error) {
    throw error
  }
}

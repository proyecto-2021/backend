import { getLastCreatureVersionById } from '@components/creatureVersion/dao'
import { message } from '@config/message'
import { handleError, handleResponse } from '@middleware/errorHandlers'
import {
  addMision,
  getAllMisions,
  getMisionById,
  getAllMisionByCreatureVersion,
  updateMisionById,
  deleteMisionById,
  activateMisionById,
  getAllMisionByCreature
} from './dao'
import {
  misionResource,
  misionsResource,
  misionResourceWithCreatureVersion,
  misionsUserResource
} from './dto'

// /v1/misions
export const listMisions = async (req, res) => {
  try {
    const creatureId = req.creatureId

    let data
    creatureId
      ? (data = await getAllMisionByCreature(creatureId))
      : (data = await getAllMisions())

    handleResponse(res, 200, message.success_long, misionsResource(data))
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/misions/version/{id}:
export const findMisionsByCreatureVersion = async (req, res) => {
  try {
    const { username } = req.query
    const data = await getAllMisionByCreatureVersion(
      req.params.creatureVersionId
    )
    let response

    if (!username) {
      response = misionsResource(data)
    } else {
      response = await misionsUserResource(data, username)
    }

    handleResponse(res, 200, message.success_long, response)
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/misions/{id}:
export const findMisionById = async (req, res) => {
  try {
    const data = await getMisionById(req.params.id)

    handleResponse(
      res,
      200,
      message.success_long,
      misionResourceWithCreatureVersion(data)
    )
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/misions
export const registerMision = async (req, res) => {
  try {
    const creatureId = req.creatureId
    const mision = req.body

    if (creatureId) {
      const creatureVersion = await getLastCreatureVersionById(creatureId)
      if (creatureVersion.length === 0)
        return handleResponse(
          res,
          400,
          'La criatura no posee versiones registradas'
        )
      else mision.creatureVersionId = creatureVersion[0].dataValues.id
    }

    const data = await addMision(mision)

    handleResponse(res, 200, message.create_success, misionResource(data))
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/misions/{id}:
export const updateMision = async (req, res) => {
  try {
    const id = req.params.id
    const mision = req.body

    const data = await updateMisionById(id, mision)

    handleResponse(
      res,
      200,
      message.update,
      misionResourceWithCreatureVersion(data)
    )
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/misions/{id}:
export const deleteMision = async (req, res) => {
  try {
    const data = await deleteMisionById(req.params.id)

    handleResponse(
      res,
      200,
      message.update,
      misionResourceWithCreatureVersion(data)
    )
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/misions/activate/{id}:
export const activateMision = async (req, res) => {
  try {
    const data = await activateMisionById(req.params.id)

    handleResponse(
      res,
      200,
      message.update,
      misionResourceWithCreatureVersion(data)
    )
  } catch (error) {
    handleError(error, res)
  }
}

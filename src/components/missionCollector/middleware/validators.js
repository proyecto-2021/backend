import { check, validationResult } from 'express-validator'
import { message } from '@config/message'
import { handleError, handleResponse } from '@middleware/errorHandlers'
import { getSubMissionById } from '@components/submission/dao'
import { getCollectorById } from '@components/collector/dao'
import { getMissionCollectorById, getMissionCollectorByIdAndUser } from '../dao'

const message_request = 'es requerido'

export const missionCollectorRequest = async (req, res, next) => {
  await check('subMissionId', `subMissionId ${message_request}`)
    .notEmpty()
    .run(req)
  await check('collectorId', `collectorId ${message_request}`)
    .notEmpty()
    .run(req)
  const request = validationResult(req)
  if (!request.isEmpty()) {
    return res.status(422).json(request)
  } else {
    next()
  }
}

export const validateMissionCollector = async (req, res, next) => {
  try {
    const { subMissionId, collectorId } = req.body
    const data = await getMissionCollectorByIdAndUser(subMissionId, collectorId)

    if (data.length > 0) return handleResponse(res, 400, message.duplicate_data)
    else next()
  } catch (error) {
    handleError(error, res)
  }
}

export const missionCollectorExistRequest = async (req, res, next) => {
  const missionCollector = await getMissionCollectorById(req.params.id)
  if (!missionCollector) {
    handleResponse(res, 400, message.not_found_long)
  } else {
    next()
  }
}

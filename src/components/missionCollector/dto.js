import { collectorResourceAlternative } from '@components/collector/dto'
import { subMissionResource } from '@components/submission/dto'

export const missionCollectorResource = (resource) => ({
  id: resource.id,
  subMissionId: resource.subMissionId,
  collectorId: resource.collectorId,
  date: resource.date
})

export const missionCollectorResourceWithMissionAndCollector = (resource) => ({
  id: resource.id,
  subMission: subMissionResource(resource.subMission),
  collector: collectorResourceAlternative(resource.collector),
  date: resource.date
})

export const missionsCollectorsResource = (resources) =>
  resources.map((resource) =>
    missionCollectorResourceWithMissionAndCollector(resource)
  )

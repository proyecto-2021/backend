import { message } from '@config/message'
import { handleError, handleResponse } from '@middleware/errorHandlers'
import {
  addMissionCollector,
  getAllMissionsCollectors,
  getMissionCollectorById,
  updateMissionCollectorById,
  deleteMissionCollectorById,
  getMissionCollectorByMissionId,
  getMissionCollectorByUsername
} from './dao'
import {
  missionCollectorResource,
  missionCollectorResourceWithMissionAndCollector,
  missionsCollectorsResource
} from './dto'

// /v1/missionsCollectors
export const listMissionsCollectors = async (req, res) => {
  try {
    let data = await getAllMissionsCollectors()

    handleResponse(
      res,
      200,
      message.success_long,
      missionsCollectorsResource(data)
    )
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/missionsCollectors/{id}:
export const findMissionCollectorById = async (req, res) => {
  try {
    const data = await getMissionCollectorById(req.params.id)

    handleResponse(
      res,
      200,
      message.success_long,
      missionCollectorResourceWithMissionAndCollector(data)
    )
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/missionsCollectors/mission/{id}:
export const findMissionCollectorByMissionId = async (req, res) => {
  try {
    const data = await getMissionCollectorByMissionId(req.params.id)

    handleResponse(
      res,
      200,
      message.success_long,
      missionsCollectorsResource(data)
    )
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/missionsCollectors/user/{username}:
export const findMissionCollectorByUsername = async (req, res) => {
  try {
    const data = await getMissionCollectorByUsername(req.params.username)

    handleResponse(
      res,
      200,
      message.success_long,
      missionsCollectorsResource(data)
    )
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/missionsCollectors
export const registerMissionCollector = async (req, res) => {
  try {
    const missionCollector = req.body

    const data = await addMissionCollector(missionCollector)

    handleResponse(
      res,
      200,
      message.create_success,
      missionCollectorResource(data)
    )
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/missionsCollectors/{id}:
export const updateMissionCollector = async (req, res) => {
  try {
    const id = req.params.id
    const MISSIONCOLLECTOR = req.body
    const DATA = await updateMissionCollectorById(id, MISSIONCOLLECTOR)
    handleResponse(res, 200, message.update, missionCollectorResource(DATA))
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/missionsCollectors/{id}:
export const deleteMissionCollector = async (req, res) => {
  try {
    await deleteMissionCollectorById(req.params.id)

    handleResponse(res, 200, message.delete)
  } catch (error) {
    handleError(error, res)
  }
}

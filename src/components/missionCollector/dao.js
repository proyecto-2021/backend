import { Op } from 'sequelize'
import {
  MissionCollectorModel,
  CollectorModel,
  SubMissionModel
} from '@services/database'

export const getAllMissionsCollectors = async () => {
  try {
    return await MissionCollectorModel.findAll({
      include: [
        {
          model: SubMissionModel,
          as: 'subMission'
        },
        {
          model: CollectorModel,
          as: 'collector'
        }
      ]
    })
  } catch (error) {
    throw error
  }
}

export const getMissionCollectorByIdAndUser = async (id, user) => {
  try {
    return await MissionCollectorModel.findAll({
      include: [
        {
          model: SubMissionModel,
          as: 'subMission'
        },
        {
          model: CollectorModel,
          as: 'collector'
        }
      ],
      where: {
        [Op.and]: [{ '$subMission.id$': id }, { '$collector.id$': user }]
      }
    })
  } catch (error) {
    throw error
  }
}

export const getMissionCollectorById = async (id) => {
  try {
    return await MissionCollectorModel.findByPk(id, {
      include: [
        {
          model: SubMissionModel,
          as: 'subMission'
        },
        {
          model: CollectorModel,
          as: 'collector'
        }
      ]
    })
  } catch (error) {
    throw error
  }
}

export const getMissionCollectorByUsername = async (username) => {
  try {
    return await MissionCollectorModel.findAll({
      include: [
        {
          model: SubMissionModel,
          as: 'subMission'
        },
        {
          model: CollectorModel,
          as: 'collector'
        }
      ],
      where: {
        '$collector.username$': String(username).toLowerCase()
      }
    })
  } catch (error) {
    throw error
  }
}

export const getMissionCollectorByMissionId = async (id) => {
  try {
    return await MissionCollectorModel.findAll({
      include: [
        {
          model: SubMissionModel,
          as: 'subMission'
        },
        {
          model: CollectorModel,
          as: 'collector'
        }
      ],
      where: {
        '$subMission.missionId$': id
      }
    })
  } catch (error) {
    throw error
  }
}

export const getMissionCollectorByMissionUser = async (missionId, username) => {
  try {
    const missions = await SubMissionModel.findAndCountAll({
      where: { missionId }
    })
    const submission = await MissionCollectorModel.findAndCountAll({
      include: [
        {
          model: SubMissionModel,
          as: 'subMission'
        },
        {
          model: CollectorModel,
          as: 'collector'
        }
      ],
      where: {
        '$subMission.missionId$': missionId,
        '$collector.username$': String(username).toLowerCase()
      }
    })
    return missions.count <= submission.count && missions.count >= 1
  } catch (error) {
    throw error
  }
}

export const getMissionCollectorByMissionUserId = async (missionId, userId) => {
  try {
    const submission = await MissionCollectorModel.findAll({
      include: [
        {
          model: SubMissionModel,
          as: 'subMission'
        },
        {
          model: CollectorModel,
          as: 'collector'
        }
      ],
      where: {
        '$subMission.missionId$': missionId,
        '$collector.id$': userId
      }
    })

    return submission
  } catch (error) {
    throw error
  }
}

export const addMissionCollector = async (missionCollector) => {
  try {
    return await MissionCollectorModel.create(missionCollector)
  } catch (error) {
    throw error
  }
}

export const updateMissionCollectorById = async (id, missionCollector) => {
  try {
    const MissionCollector = await MissionCollectorModel.findByPk(id, {
      include: [
        {
          model: SubMissionModel,
          as: 'subMission'
        },
        {
          model: CollectorModel,
          as: 'collector'
        }
      ]
    })
    return MissionCollector.update(missionCollector)
  } catch (error) {
    throw error
  }
}

export const deleteMissionCollectorById = async (id) => {
  try {
    await MissionCollectorModel.destroy({ where: { id } })
  } catch (error) {
    throw error
  }
}

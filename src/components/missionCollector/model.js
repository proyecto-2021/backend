import { DataTypes } from 'sequelize'

export const dataMissionCollector = {
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  subMissionId: {
    type: DataTypes.INTEGER,
    allowNull: false
  },
  collectorId: {
    type: DataTypes.INTEGER,
    allowNull: false
  },
  date: {
    type: DataTypes.DATEONLY,
    defaultValue: DataTypes.NOW
  }
}

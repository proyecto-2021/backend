import { usernameCollectorExistRequest } from '@components/collector/middleware/validators'
import { misionExistRequest } from '@components/mision/middleware/validators'
import { Router } from 'express'
import {
  deleteMissionCollector,
  findMissionCollectorById,
  findMissionCollectorByMissionId,
  findMissionCollectorByUsername,
  listMissionsCollectors,
  registerMissionCollector,
  updateMissionCollector
} from './controller'
import {
  missionCollectorExistRequest,
  missionCollectorRequest,
  validateMissionCollector
} from './middleware/validators'

const router = Router()

router.get('/', listMissionsCollectors)
router.get('/:id', [missionCollectorExistRequest], findMissionCollectorById)
router.get(
  '/mission/:id',
  [misionExistRequest],
  findMissionCollectorByMissionId
)
router.get(
  '/user/:username',
  [usernameCollectorExistRequest],
  findMissionCollectorByUsername
)
router.post(
  '/',
  [missionCollectorRequest, validateMissionCollector],
  registerMissionCollector
)
router.put('/:id', [missionCollectorExistRequest], updateMissionCollector)
router.delete('/:id', [missionCollectorExistRequest], deleteMissionCollector)

export const missionCollectorsRoutes = router

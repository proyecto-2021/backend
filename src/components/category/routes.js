import { Router } from 'express'
import {
  listCategories,
  registerCategory,
  updateCategory,
  deleteCategory,
  findCategoryById,
  activeCategory
} from './controller'
import {
  categoryExistRequest,
  categoryNameExistRequest,
  categoryRequest
} from './middleware/validators'

const router = Router()
router.get('/', listCategories)
router.post('/', [categoryRequest, categoryNameExistRequest], registerCategory)
router.put(
  '/:id',
  [categoryRequest, categoryExistRequest, categoryNameExistRequest],
  updateCategory
)
router.put('/active/:id', [categoryExistRequest], activeCategory)
router.delete('/:id', [categoryExistRequest], deleteCategory)
router.get('/:id', [categoryExistRequest], findCategoryById)

export const categoryRoutes = router

export const categoryResource = (resource) => ({
  id: resource.id,
  name: resource.name,
  description: resource.description,
  isActive: resource.isActive
})

export const categoriesResource = (resources) =>
  resources.map((resource) => categoryResource(resource))

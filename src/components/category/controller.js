import { message } from '@config/message'
import { handleError, handleResponse } from '@middleware/errorHandlers'
import {
  getAllCategories,
  addCategory,
  updateCategoryById,
  deleteCategoryById,
  getCategoryById,
  activeCategoryById
} from './dao'
import { categoriesResource, categoryResource } from './dto'

// /v1/categories
export const listCategories = async (req, res) => {
  try {
    const data = await getAllCategories()
    handleResponse(res, 200, message.success_long, categoriesResource(data))
  } catch (error) {
    handleError(error, res)
  }
}

// v1/category
export const registerCategory = async (req, res) => {
  try {
    const category = req.body

    const data = await addCategory(category)

    handleResponse(res, 200, message.create_success, categoryResource(data))
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/category/{id}:
export const updateCategory = async (req, res) => {
  try {
    const id = req.params.id
    const category = req.body

    const data = await updateCategoryById(id, category)

    handleResponse(res, 200, message.update, categoryResource(data))
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/category/{id}:
export const findCategoryById = async (req, res) => {
  try {
    const data = await getCategoryById(req.params.id)
    const response = await categoryResource(data)

    handleResponse(res, 200, message.success_long, response)
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/categories/{id}:
export const deleteCategory = async (req, res) => {
  try {
    const id = req.params.id
    const data = await deleteCategoryById(id)
    handleResponse(res, 200, message.delete, categoryResource(data))
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/categories/active/{id}:
export const activeCategory = async (req, res) => {
  try {
    const id = req.params.id
    const data = await activeCategoryById(id)
    handleResponse(res, 200, message.success, categoryResource(data))
  } catch (error) {
    handleError(error, res)
  }
}

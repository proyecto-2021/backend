import { check, validationResult } from 'express-validator'
import { message } from '@config/message'
import { handleError, handleResponse } from '@middleware/errorHandlers'

import { getCategoryById, getCategoryByName } from '../dao'

const message_request = 'es requerido'

export const categoryRequest = async (req, res, next) => {
  await check('name', `name ${message_request}`).notEmpty().run(req)
  await check('description', `description ${message_request}`)
    .notEmpty()
    .run(req)

  const request = validationResult(req)
  if (!request.isEmpty()) {
    return res.status(422).json(request)
  } else {
    next()
  }
}

export const categoryNameExistRequest = async (req, res, next) => {
  try {
    const category = await getCategoryByName(req.body.name)

    if (
      (category && !req.params.id) ||
      (req.params.id && category && category.id != req.params.id)
    ) {
      handleResponse(res, 400, message.duplicate_data)
    } else {
      next()
    }
  } catch (error) {
    handleError(error, res)
  }
}

export const categoryExistRequest = async (req, res, next) => {
  const category = await getCategoryById(req.params.id)

  if (!category) {
    handleResponse(res, 400, message.not_found_long)
  } else {
    next()
  }
}

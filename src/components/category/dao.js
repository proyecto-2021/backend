import { CategoryModel } from '@services/database'
import { db } from '@services/database'

export const getAllCategories = async () => {
  try {
    return await CategoryModel.findAll()
  } catch (error) {
    throw error
  }
}

export const getCategoryById = async (id) => {
  try {
    return await CategoryModel.findByPk(id)
  } catch (error) {
    throw error
  }
}

export const getCategoryByName = async (name) => {
  try {
    return await CategoryModel.findOne({
      where: db.Sequelize.where(
        db.Sequelize.fn('lower', db.Sequelize.col('name')),
        db.Sequelize.fn('lower', name)
      )
    })
  } catch (error) {
    throw error
  }
}

export const addCategory = async (category) => {
  try {
    return await CategoryModel.create(category)
  } catch (error) {
    throw error
  }
}

export const updateCategoryById = async (id, category) => {
  try {
    const Category = await CategoryModel.findByPk(id)
    return Category.update(category)
  } catch (error) {
    throw error
  }
}

export const deleteCategoryById = async (id) => {
  try {
    const Category = await CategoryModel.findByPk(id)
    return Category.update({ isActive: false })
  } catch (error) {
    throw error
  }
}

export const activeCategoryById = async (id) => {
  try {
    const Category = await CategoryModel.findByPk(id)
    return Category.update({ isActive: true })
  } catch (error) {
    throw error
  }
}

import { getFavoritesByCreature } from '@components/favorite/dao'
import { addNotification } from '@components/notification/dao'
import { message } from '@config/message'
import { handleError, handleResponse } from '@middleware/errorHandlers'
import {
  addCreatureVersion,
  deleteCreatureVersionById,
  getAllCreatureVersions,
  getAllCreatureVersionsByCreature,
  getCreatureVersionById,
  updateCreatureVersionById
} from './dao'
import {
  creatureVersionResource,
  creatureVersionResourceWithoutCreature,
  creatureVersionsResource,
  creatureVersionsResourceAlternative
} from './dto'

// /v1/creatureVersions
export const listCreatureVersions = async (req, res) => {
  try {
    const creatureId = req.creatureId
    let data

    creatureId
      ? (data = await getAllCreatureVersionsByCreature(creatureId))
      : (data = await getAllCreatureVersions())

    handleResponse(
      res,
      200,
      message.success_long,
      creatureVersionsResource(data)
    )
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/creatureVersions/{id}:
export const findCreatureVersionById = async (req, res) => {
  try {
    const data = await getCreatureVersionById(req.params.id)

    handleResponse(
      res,
      200,
      message.success_long,
      creatureVersionResource(data)
    )
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/creatureVersions/creature/{creatureId}:
export const findCreatureVersionsByCreature = async (req, res) => {
  try {
    const data = await getAllCreatureVersionsByCreature(req.params.creatureId)

    handleResponse(
      res,
      200,
      message.success_long,
      creatureVersionsResourceAlternative(data)
    )
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/creatureVersions
export const registerCreatureVersion = async (req, res) => {
  try {
    const creatureId = req.creatureId
    const creatureVersion = req.body

    if (creatureId) creatureVersion.creatureId = creatureId

    const data = await addCreatureVersion(creatureVersion)

    const users = await getFavoritesByCreature(creatureVersion.creatureId)

    users.forEach(async (user) => {
      const notification = {
        collectorId: user.collectorId,
        name: '¡Nueva versión disponible de una criatura!',
        description: `Ya se encuentra disponible una nueva versión de la criatura: ${user.creature.username}`
      }
      await addNotification(notification)
    })

    handleResponse(
      res,
      200,
      message.create_success,
      creatureVersionResourceWithoutCreature(data)
    )
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/creatureVersions/{id}:
export const updateCreatureVersion = async (req, res) => {
  try {
    const id = req.params.id
    const creatureVersion = req.body

    const data = await updateCreatureVersionById(id, creatureVersion)

    handleResponse(res, 200, message.update, creatureVersionResource(data))
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/creatureVersions/{id}:
export const deleteCreatureVersion = async (req, res) => {
  try {
    const data = await deleteCreatureVersionById(req.params.id)

    handleResponse(res, 200, message.update, creatureVersionResource(data))
  } catch (error) {
    handleError(error, res)
  }
}

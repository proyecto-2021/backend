import { check, validationResult } from 'express-validator'
import { message } from '@config/message'
import { handleResponse } from '@middleware/errorHandlers'
import { getCreatureById } from '@components/creature/dao'
import { getCreatureVersionById } from '../dao'

const message_request = 'es requerido'

export const verifyUserInCreatureVersion = async (req, res, next) => {
  const { id, type, creatureId, roleId } = req.token
  const { method } = req

  if (type === 'A') {
    next()
  } else if (type === 'C' || roleId === 1) {
    if (!req.params.id && !req.params.creatureId) {
      req.creatureId = type === 'C' ? id : creatureId
      next()
    } else if (req.params.id) {
      const creatureV = await getCreatureVersionById(req.params.id)

      if (
        creatureV?.creatureId === id ||
        creatureV?.creatureId === creatureId
      ) {
        next()
      } else {
        handleResponse(res, 403, message.forbidden_long)
      }
    } else {
      handleResponse(res, 403, message.forbidden_long)
    }
  } else if (type === 'W') {
    if (method === 'GET') {
      if (!req.params.id) {
        next()
      } else if (req.params.id) {
        const creatureV = await getCreatureVersionById(req.params.id)

        if (creatureV?.creatureId === creatureId) next()
        else handleResponse(res, 403, message.forbidden_long)
      } else handleResponse(res, 403, message.forbidden_long)
    } else handleResponse(res, 403, message.forbidden_long)
  } else {
    method === 'GET' ? next() : handleResponse(res, 403, message.forbidden_long)
  }
}

export const creatureVersionRequest = async (req, res, next) => {
  await check('name', `name ${message_request}`).notEmpty().run(req)
  await check('url', `url ${message_request}`).notEmpty().run(req)

  const request = validationResult(req)
  if (!request.isEmpty()) {
    return res.status(422).json(request)
  } else {
    next()
  }
}

export const creatureVersionExistRequest = async (req, res, next) => {
  const creatureVersion = await getCreatureVersionById(req.params.id)

  if (!creatureVersion) {
    handleResponse(res, 400, message.not_found_long)
  } else {
    next()
  }
}

export const creatureIdCreatureVersionExistRequest = async (req, res, next) => {
  const creature = await getCreatureById(req.params.creatureId)

  if (!creature) {
    handleResponse(res, 400, message.not_found_long)
  } else {
    next()
  }
}

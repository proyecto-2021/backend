import { CreatureModel } from '@services/database'
import { CreatureVersionModel } from '@services/database'
import { delCascadeMisionByCreatureVersion } from '@components/mision/dao'

export const getAllCreatureVersions = async () => {
  try {
    return await CreatureVersionModel.findAll({
      include: {
        model: CreatureModel,
        as: 'creature'
      }
    })
  } catch (error) {
    throw error
  }
}

export const getAllCreatureVersionsByCreature = async (creatureId) => {
  try {
    return await CreatureVersionModel.findAll({
      include: {
        model: CreatureModel,
        as: 'creature'
      },
      where: {
        creatureId
      }
    })
  } catch (error) {
    throw error
  }
}

export const getCreatureVersionById = async (id) => {
  try {
    return await CreatureVersionModel.findByPk(id, {
      include: {
        model: CreatureModel,
        as: 'creature'
      }
    })
  } catch (error) {
    throw error
  }
}

export const addCreatureVersion = async (creature) => {
  try {
    return await CreatureVersionModel.create(creature)
  } catch (error) {
    throw error
  }
}

export const updateCreatureVersionById = async (id, creature) => {
  try {
    const { name, url } = creature
    const CreatureVersion = await CreatureVersionModel.findByPk(id, {
      include: {
        model: CreatureModel,
        as: 'creature'
      }
    })
    return await CreatureVersion.update({ name, url })
  } catch (error) {
    throw error
  }
}

export const deleteCreatureVersionById = async (id) => {
  try {
    const CreatureVersion = await CreatureVersionModel.findByPk(id, {
      include: {
        model: CreatureModel,
        as: 'creature'
      }
    })
    const data = await CreatureVersion.update({ isActive: false })

    await delCascadeMisionByCreatureVersion(id)

    return data
  } catch (error) {
    throw error
  }
}

export const getLastCreatureVersionById = async (creatureId) => {
  try {
    return await CreatureVersionModel.findAll({
      order: [['id', 'DESC']],
      where: {
        creatureId
      },
      limit: 1
    })
  } catch (error) {
    throw error
  }
}

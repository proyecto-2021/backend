export const creatureVersionResource = (resource) => ({
  id: resource.id,
  name: resource.name,
  url: resource.url,
  date: resource.date,
  isActive: resource.isActive,
  creature: resource.creature
})

export const creatureVersionResourceAlternative = (resource) => ({
  id: resource.id,
  name: resource.name,
  url: resource.url,
  date: resource.date,
  isActive: resource.isActive,
  creature: resource.creature.dataValues
})

export const creatureVersionResourceWithoutCreature = (resource) => ({
  id: resource.id,
  creatureId: resource.creatureId,
  name: resource.name,
  url: resource.url,
  date: resource.date,
  isActive: resource.isActive
})

export const creatureVersionsResource = (resources) =>
  resources.map((resource) => creatureVersionResource(resource))

export const creatureVersionsResourceAlternative = (resources) =>
  resources.map((resource) => creatureVersionResourceAlternative(resource))

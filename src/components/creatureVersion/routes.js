import { Router } from 'express'
import {
  deleteCreatureVersion,
  findCreatureVersionsByCreature,
  findCreatureVersionById,
  listCreatureVersions,
  registerCreatureVersion,
  updateCreatureVersion
} from './controller'
import {
  creatureIdCreatureVersionExistRequest,
  creatureVersionRequest,
  creatureVersionExistRequest,
  verifyUserInCreatureVersion
} from './middleware/validators'

const router = Router()

router.get('/', verifyUserInCreatureVersion, listCreatureVersions)
router.get(
  '/:id',
  [verifyUserInCreatureVersion, creatureVersionExistRequest],
  findCreatureVersionById
)
router.get(
  '/creature/:creatureId',
  [verifyUserInCreatureVersion, creatureIdCreatureVersionExistRequest],
  findCreatureVersionsByCreature
)
router.post(
  '/',
  [creatureVersionRequest, verifyUserInCreatureVersion],
  registerCreatureVersion
)
router.put(
  '/:id',
  [
    verifyUserInCreatureVersion,
    creatureVersionExistRequest,
    creatureVersionRequest
  ],
  updateCreatureVersion
)
router.delete(
  '/:id',
  [verifyUserInCreatureVersion, creatureVersionExistRequest],
  deleteCreatureVersion
)

export const creatureVersionRoutes = router

import { check, validationResult } from 'express-validator'
import { message } from '@config/message'
import { handleError, handleResponse } from '@middleware/errorHandlers'
import { getTaskById } from '../dao'
import { getWizardById } from '@components/wizard/dao'
import { getReportById } from '@components/report/dao'

const message_request = 'es requerido'

export const verifyUserInTask = async (req, res, next) => {
  const { type, creatureId, id, roleId } = req.token
  const { method } = req

  if (type === 'A') {
    next()
  } else if (type === 'W') {
    if (!req.params.id) {
      next()
    } else if (req.params.id) {
      ///////
      const task = await getTaskById(req.params.id)
      const report = await getReportById(task.report.id)
      const creature = report.creatureVersion

      if (creature?.creatureId === creatureId) {
        next()
      } else {
        handleResponse(res, 403, message.forbidden_long)
      }
      ///////
    } else {
      handleResponse(res, 403, message.forbidden_long)
    }
  } else {
    handleResponse(res, 403, message.forbidden_long)
  }
}

export const taskRequest = async (req, res, next) => {
  // await check('wizardId', `wizardId ${message_request}`).notEmpty().run(req)
  await check('reportId', `reportId ${message_request}`).notEmpty().run(req)
  await check('name', `name ${message_request}`).notEmpty().run(req)
  // await check('description', `description ${message_request}`).notEmpty().run(req)
  // await check('date', `date ${message_request}`).notEmpty().run(req)

  const request = validationResult(req)
  if (!request.isEmpty()) {
    return res.status(422).json(request)
  } else {
    next()
  }
}

export const taskExistRequest = async (req, res, next) => {
  const task = await getTaskById(req.params.id)

  if (!task) {
    handleResponse(res, 400, message.not_found_long)
  } else {
    next()
  }
}

export const wizardExist = async (req, res, next) => {
  try {
    const wizard = await getWizardById(req.body.wizardId)

    if (!wizard) {
      handleResponse(res, 404, 'El mago no existe')
    } else {
      next()
    }
  } catch (error) {
    handleError(error, res)
  }
}

export const reportExistInTask = async (req, res, next) => {
  try {
    const report = await getReportById(req.body.reportId)

    if (!report) {
      handleResponse(res, 404, 'El reporte no existe')
    } else {
      next()
    }
  } catch (error) {
    handleError(error, res)
  }
}

export const taskStatusRequest = async (req, res, next) => {
  await check('status', `status ${message_request}`)
    .notEmpty()
    .isIn(['P', 'PR', 'PB', 'PO'])
    .withMessage('estatus inválido')
    .run(req)

  const request = validationResult(req)
  if (!request.isEmpty()) {
    return res.status(422).json(request)
  } else {
    next()
  }
}

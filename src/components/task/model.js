import { DataTypes } from 'sequelize'
import { v4 as uuidv4 } from 'uuid'

export const dataTask = {
  // id: {
  //   type: DataTypes.UUID,
  //   primaryKey: true,
  //   defaultValue: uuidv4()
  // },
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  wizardId: {
    type: DataTypes.INTEGER,
    allowNull: false
  },
  reportId: {
    type: DataTypes.INTEGER,
    allowNull: true
  },
  name: {
    type: DataTypes.STRING,
    allowNull: false
  },
  description: {
    type: DataTypes.STRING
  },
  date: {
    type: DataTypes.DATEONLY
  },
  status: {
    type: DataTypes.STRING,
    allowNull: false,
    defaultValue: 'P', //P = pendiente , PR = proceso, PB = pruebas y PO = procesado
    enum: ['P', 'PR', 'PB', 'PO']
  }
}

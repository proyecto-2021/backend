export const taskResource = (resource) => ({
  id: resource.id,
  name: resource.name,
  description: resource.description,
  date: resource.date,
  status: resource.status,
  wizard: resource.wizard || resource.wizardId,
  report: resource.report || resource.reportId
})

export const taskResourceAlternative = (resource) => ({
  id: resource.id,
  name: resource.name,
  description: resource.description,
  date: resource.date,
  status: resource.status,
  wizardId: resource.wizardId,
  reportId: resource.reportId
})

export const tasksResource = (resources) =>
  resources.map((resource) => taskResource(resource))

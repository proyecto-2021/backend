import { message } from '@config/message'
import { handleError, handleResponse } from '@middleware/errorHandlers'
import {
  getAllTasks,
  getTaskById,
  updateTaskById,
  addTask,
  deleteTaskById,
  getAllTasksByWizard,
  getAllTasksByCreature
} from './dao'
import { taskResource, taskResourceAlternative, tasksResource } from './dto'

export const listTasks = async (req, res) => {
  try {
    const { type, roleId, creatureId, id } = req.token
    let data

    if (type === 'A') data = await getAllTasks()
    else if (roleId === 1) data = await getAllTasksByCreature(creatureId)
    else data = await getAllTasksByWizard(id)

    handleResponse(res, 200, message.success_long, tasksResource(data))
  } catch (error) {
    handleError(error, res)
  }
}

export const findTaskById = async (req, res) => {
  try {
    const data = await getTaskById(req.params.id)

    handleResponse(res, 200, message.success_long, taskResource(data))
  } catch (error) {
    handleError(error, res)
  }
}

export const registerTask = async (req, res) => {
  try {
    const task = req.body
    const { type, id } = req.token

    if (type === 'W' && !task.wizardId) task.wizardId = id

    const data = await addTask(task)

    handleResponse(
      res,
      200,
      message.create_success,
      taskResourceAlternative(data)
    )
  } catch (error) {
    handleError(error, res)
  }
}

export const updateTask = async (req, res) => {
  try {
    const id = req.params.id
    const task = req.body

    const data = await updateTaskById(id, task)

    handleResponse(res, 200, message.update, taskResource(data))
  } catch (error) {
    handleError(error, res)
  }
}

export const deleteTask = async (req, res) => {
  try {
    const id = req.params.id
    const data = await deleteTaskById(id)
    handleResponse(res, 200, message.delete, taskResource(data))
  } catch (error) {
    handleError(error, res)
  }
}

import { Router } from 'express'
import {
  deleteTask,
  findTaskById,
  listTasks,
  registerTask,
  updateTask
} from './controller'
import {
  reportExistInTask,
  taskExistRequest,
  taskRequest,
  verifyUserInTask,
  wizardExist
} from './middleware/validators'

const router = Router()

router.get('/', verifyUserInTask, listTasks)
router.get('/:id', [taskExistRequest, verifyUserInTask], findTaskById)
router.post(
  '/',
  [taskRequest, verifyUserInTask, wizardExist, reportExistInTask],
  registerTask
)
router.put(
  '/:id',
  [taskExistRequest, verifyUserInTask, wizardExist, reportExistInTask],
  updateTask
)
router.delete('/:id', [taskExistRequest, verifyUserInTask], deleteTask)
export const taskRoutes = router

import {
  WizardModel,
  TaskModel,
  ReportModel,
  CreatureVersionModel,
  db
} from '@services/database'

export const getAllTasks = async () => {
  try {
    return await TaskModel.findAll({
      include: [
        {
          model: WizardModel,
          as: 'wizard'
        },
        {
          model: ReportModel,
          as: 'report'
        }
      ]
    })
  } catch (error) {
    throw error
  }
}

export const getAllTasksByWizard = async (wizardId) => {
  try {
    return await TaskModel.findAll({
      include: [
        {
          model: WizardModel,
          as: 'wizard'
        },
        {
          model: ReportModel,
          as: 'report'
        }
      ],
      where: { wizardId }
    })
  } catch (error) {
    throw error
  }
}

export const getAllTasksByCreature = async (creatureId) => {
  try {
    // `SELECT
    // task.id,
    // task."wizardId",
    // task."reportId",
    // task.name,
    // task.description,
    // task.date,
    // task.status,
    // to_json (report) "report"
    // FROM tasks task, reports report, "creatureVersions" "creatureVersion"
    // WHERE task."reportId"=report.id
    // AND report."creatureVersionId" = "creatureVersion".id
    // AND "creatureVersion"."creatureId"=${creatureId};`
    const data = await db.query(
      `SELECT
        task.id,
        task."wizardId",
        task."reportId",
        task.name,
        task.description,
        task.date,
        task.status,
        to_json (report) "report",
        to_json (wizard) "wizard"
      FROM tasks task
      INNER JOIN reports report ON
        report.id = task."reportId"
      INNER JOIN "creatureVersions" "creatureVersion" ON
        report."creatureVersionId" = "creatureVersion".id AND
        "creatureVersion"."creatureId"=${creatureId}
      INNER JOIN wizards wizard ON
        wizard.id = task."wizardId";`
    )
    return data[0]
  } catch (error) {
    throw error
  }
}

export const getTaskById = async (id) => {
  try {
    return await TaskModel.findByPk(id, {
      include: [
        {
          model: WizardModel,
          as: 'wizard'
        },
        {
          model: ReportModel,
          as: 'report'
        }
      ]
    })
  } catch (error) {
    throw error
  }
}

export const addTask = async (task) => {
  try {
    return await TaskModel.create(task)
  } catch (error) {
    throw error
  }
}

export const updateTaskById = async (id, task) => {
  try {
    const Task = await TaskModel.findByPk(id, {
      include: [
        {
          model: WizardModel,
          as: 'wizard'
        },
        {
          model: ReportModel,
          as: 'report'
        }
      ]
    })
    return Task.update(task)
  } catch (error) {
    throw error
  }
}

export const deleteTaskById = async (id) => {
  try {
    const Task = await TaskModel.findByPk(id)
    await Task.destroy()
    return Task.dataValues
  } catch (error) {
    throw error
  }
}

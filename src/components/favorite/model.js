import { DataTypes } from 'sequelize'
import { v4 as uuidv4 } from 'uuid'

export const dataFavorite = {
  // id: {
  //   type: DataTypes.UUID,
  //   primaryKey: true,
  //   defaultValue: uuidv4()
  // },
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  collectorId: {
    type: DataTypes.INTEGER,
    allowNull: false
  },
  creatureId: {
    type: DataTypes.INTEGER,
    allowNull: false
  },
  date: {
    type: DataTypes.DATEONLY,
    defaultValue: DataTypes.NOW
  }
}

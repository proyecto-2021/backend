import {
  FavoriteModel,
  CollectorModel,
  CreatureModel
} from '@services/database'

export const getAllFavorites = async () => {
  try {
    return await FavoriteModel.findAll({
      include: [
        {
          model: CollectorModel,
          as: 'collector'
        },
        {
          model: CreatureModel,
          as: 'creature'
        }
      ]
    })
  } catch (error) {
    throw error
  }
}

export const getFavoriteById = async (id) => {
  try {
    return await FavoriteModel.findByPk(id, {
      include: [
        {
          model: CollectorModel,
          as: 'collector'
        },
        {
          model: CreatureModel,
          as: 'creature'
        }
      ]
    })
  } catch (error) {
    throw error
  }
}

export const getFavoritesByCollector = async (collectorId) => {
  try {
    return await FavoriteModel.findAll({
      where: {
        collectorId
      },
      include: [
        {
          model: CollectorModel,
          as: 'collector'
        },
        {
          model: CreatureModel,
          as: 'creature'
        }
      ]
    })
  } catch (error) {
    throw error
  }
}

export const getFavoritesByCreature = async (creatureId) => {
  try {
    return await FavoriteModel.findAll({
      where: {
        creatureId
      },
      include: [
        {
          model: CollectorModel,
          as: 'collector'
        },
        {
          model: CreatureModel,
          as: 'creature'
        }
      ]
    })
  } catch (error) {
    throw error
  }
}

export const getFavoritesByCollectorCreature = async (
  collectorId,
  creatureId
) => {
  try {
    return await FavoriteModel.findOne({
      where: {
        collectorId,
        creatureId
      }
    })
  } catch (error) {
    throw error
  }
}

export const addFavorite = async (favorite) => {
  try {
    return await FavoriteModel.create(favorite)
  } catch (error) {
    throw error
  }
}

export const updateFavoriteById = async (id, favorite) => {
  try {
    const Favorite = await FavoriteModel.findByPk(id)
    return Favorite.update(favorite)
  } catch (error) {
    throw error
  }
}

export const deleteFavoriteById = async (id) => {
  try {
    const Favorite = await FavoriteModel.findByPk(id, {
      include: [
        {
          model: CollectorModel,
          as: 'collector'
        },
        {
          model: CreatureModel,
          as: 'creature'
        }
      ]
    })
    return Favorite.destroy()
  } catch (error) {
    throw error
  }
}

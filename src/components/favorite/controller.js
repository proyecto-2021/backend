import { message } from '@config/message'
import { handleError, handleResponse } from '@middleware/errorHandlers'
import {
  addFavorite,
  deleteFavoriteById,
  getAllFavorites,
  getFavoriteById,
  getFavoritesByCollector,
  updateFavoriteById
} from './dao'
import {
  favoriteResource,
  favoriteResourceAlternative,
  favoritesResource
} from './dto'

// /v1/favorites
export const listFavorites = async (req, res) => {
  try {
    const { type, id } = req.token

    let data

    type === 'CO'
      ? (data = await getFavoritesByCollector(id))
      : (data = await getAllFavorites())

    handleResponse(res, 200, message.success_long, favoritesResource(data))
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/favorites/{id}:
export const findFavoriteById = async (req, res) => {
  try {
    const data = await getFavoriteById(req.params.id)

    handleResponse(res, 200, message.success_long, favoriteResource(data))
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/favorites/collector/{collectorId}:
export const findFavoritesByCollector = async (req, res) => {
  try {
    const data = await getFavoritesByCollector(req.params.collectorId)

    if (data.length != 0) {
      handleResponse(res, 200, message.success_long, favoritesResource(data))
    } else {
      handleResponse(res, 200, message.no_content)
    }
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/favorites
export const registerFavorite = async (req, res) => {
  try {
    const { type, id } = req.token
    const favorite = req.body

    if (type === 'CO') favorite.collectorId = id

    const data = await addFavorite(favorite)

    handleResponse(
      res,
      200,
      message.create_success,
      favoriteResourceAlternative(data)
    )
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/favorites
export const deleteFavorite = async (req, res) => {
  try {
    const id = req.params.id

    const data = await deleteFavoriteById(id)

    handleResponse(
      res,
      200,
      message.response_success_long,
      favoriteResource(data)
    )
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/favorites/{id}:
export const updateFavorite = async (req, res) => {
  try {
    const id = req.params.id
    const favorite = req.body

    const data = await updateFavoriteById(id, favorite)

    handleResponse(res, 200, message.update, favoriteResource(data))
  } catch (error) {
    handleError(error, res)
  }
}

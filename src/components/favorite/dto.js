export const favoriteResource = (resource) => ({
  id: resource.id,
  collector: resource.collector,
  creature: resource.creature,
  date: resource.date
})

export const favoriteResourceAlternative = (resource) => ({
  id: resource.id,
  collectorId: resource.collectorId,
  creatureId: resource.creatureId,
  date: resource.date
})

export const favoritesResource = (resources) =>
  resources.map((resource) => favoriteResource(resource))

import { check, validationResult } from 'express-validator'
import { message } from '@config/message'
import { handleResponse } from '@middleware/errorHandlers'
import { getCollectorById } from '@components/collector/dao'

import {
  getFavoriteById,
  getFavoritesByCollector,
  getFavoritesByCollectorCreature
} from '../dao'
import { getCreatureById } from '@components/creature/dao'

const message_request = 'es requerido'

export const favoriteRequest = async (req, res, next) => {
  await check('creatureId', `creatureId ${message_request}`).notEmpty().run(req)

  const request = validationResult(req)
  if (!request.isEmpty()) {
    return res.status(422).json(request)
  } else {
    next()
  }
}

export const favoriteExistRequest = async (req, res, next) => {
  const { id, type } = req.token
  const favorite = await getFavoriteById(req.params.id)

  if (favorite?.collectorId === id || type === 'A') {
    next()
  } else {
    handleResponse(res, 400, message.not_found_long)
  }
}

export const checkfavoriteExist = async (req, res, next) => {
  const { id, type } = req.token
  let favorite

  type === 'A'
    ? (favorite = await getFavoritesByCollectorCreature(
        req.body.collectorId,
        req.body.creatureId
      ))
    : (favorite = await getFavoritesByCollectorCreature(
        id,
        req.body.creatureId
      ))

  if (!favorite) {
    next()
  } else {
    handleResponse(res, 400, message.duplicate_data)
  }
}

export const collectorFavoritesExistRequest = async (req, res, next) => {
  const { id, type } = req.token

  const collector = await getCollectorById(req.params.collectorId)
  const favorite = await getFavoritesByCollector(req.params.collectorId)

  if ((favorite && collector?.id === id) || type === 'A') {
    next()
  } else handleResponse(res, 400, message.not_found_long)
}

export const creatureIdExistRequest = async (req, res, next) => {
  const creature = await getCreatureById(req.body.creatureId)

  if (!creature) {
    handleResponse(res, 400, `creatureId: ${message.not_found_long}`)
  } else {
    next()
  }
}

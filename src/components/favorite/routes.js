import { Router } from 'express'
import {
  findFavoritesByCollector,
  findFavoriteById,
  listFavorites,
  registerFavorite,
  updateFavorite,
  deleteFavorite
} from './controller'
import {
  favoriteExistRequest,
  favoriteRequest,
  collectorFavoritesExistRequest,
  creatureIdExistRequest,
  checkfavoriteExist
} from './middleware/validators'

const router = Router()

router.get('/', listFavorites)
router.get('/:id', favoriteExistRequest, findFavoriteById)
router.get(
  '/collector/:collectorId',
  collectorFavoritesExistRequest,
  findFavoritesByCollector
)
router.post(
  '/',
  [favoriteRequest, creatureIdExistRequest, checkfavoriteExist],
  registerFavorite
)
router.put(
  '/:id',
  [favoriteExistRequest, favoriteRequest, creatureIdExistRequest],
  updateFavorite
)
router.delete('/:id', favoriteExistRequest, deleteFavorite)

export const favoriteRoutes = router

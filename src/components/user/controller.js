import bcrypt from 'bcryptjs'
import { message } from '@config/message'
import { handleError, handleResponse } from '@middleware/errorHandlers'
import {
  addUser,
  getAllUsers,
  getUserByUsername,
  updateUserByUname,
  deleteUserByUname,
  reactivateUserByUname,
  updatePwd,
  getAllUsersByType
} from './dao'
import {
  userNoPasswordResource,
  userResource,
  usersNoPasswordResource
} from './dto'
import { deleteCascadeCreatureByUsername } from '@components/creature/dao'
import { deleteCasacadeCollectorByUsername } from '@components/collector/dao'
import { deleteCasacadeWizardByUsername } from '@components/wizard/dao'

// /v1/users
export const listUsers = async (req, res) => {
  try {
    const data = await getAllUsers()

    handleResponse(
      res,
      200,
      message.success_long,
      usersNoPasswordResource(data)
    )
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/user/type/{type}:
export const listUsersByType = async (req, res) => {
  try {
    const data = await getAllUsersByType(req.params.type)

    handleResponse(
      res,
      200,
      message.success_long,
      usersNoPasswordResource(data)
    )
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/user/{username}:
export const findUserByUsername = async (req, res) => {
  try {
    const data = await getUserByUsername(req.params.username)

    handleResponse(res, 200, message.success_long, userNoPasswordResource(data))
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/user/profile/:
export const findProfileByUsername = async (req, res) => {
  try {
    const { username } = req.token
    const data = await getUserByUsername(username)

    handleResponse(res, 200, message.success_long, userNoPasswordResource(data))
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/user
export const registerUser = async (req, res) => {
  try {
    let user = req.body

    const salt = await bcrypt.genSalt(10)
    const passwordEncrip = await bcrypt.hash(user.password, salt)
    user.password = passwordEncrip
    user.type = 'A'

    await addUser(user)

    handleResponse(res, 200, message.create_success)
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/user/{username}:
export const updateUserByUsername = async (req, res) => {
  try {
    const username = req.params.username
    let user = req.body

    if (user.password) {
      const salt = await bcrypt.genSalt(10)
      const passwordEncrip = await bcrypt.hash(user.password, salt)
      user.password = passwordEncrip
    }

    const data = await updateUserByUname(username, user)

    handleResponse(res, 200, message.update, userNoPasswordResource(data))
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/user/{username}:
export const deleteUserByUsername = async (req, res) => {
  try {
    const id = req.params.username

    const data = await deleteUserByUname(id)

    switch (data.type) {
      case 'C':
        await deleteCascadeCreatureByUsername(id)
        break
      case 'CO':
        await deleteCasacadeCollectorByUsername(id)
        break
      case 'W':
        await deleteCasacadeWizardByUsername(id)
        break
      default:
        break
    }

    handleResponse(res, 200, message.success_long, userNoPasswordResource(data))
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/user/{username}:
export const reactivateUserByUsername = async (req, res) => {
  try {
    const id = req.params.username

    const data = await reactivateUserByUname(id)

    handleResponse(res, 200, message.success_long, userNoPasswordResource(data))
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/user/updatePassword/{username} && /v1/user/updateMyPassword:
export const updatePassword = async (req, res) => {
  try {
    const username = req.params.username
      ? req.params.username
      : req.token.username

    const newPassword = req.body.newPassword
      ? req.body.newPassword
      : req.body.password

    console.log(username)
    console.log(newPassword)

    const salt = await bcrypt.genSalt(10)
    const passwordEncrip = await bcrypt.hash(newPassword, salt)

    await updatePwd(username, passwordEncrip)

    handleResponse(res, 200, message.success_long)
  } catch (error) {
    handleError(error, res)
  }
}

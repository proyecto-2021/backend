import { DataTypes } from 'sequelize'

export const dataUser = {
  username: {
    type: DataTypes.STRING,
    primaryKey: true
  },
  firstName: {
    type: DataTypes.STRING
  },
  lastName: {
    type: DataTypes.STRING
  },
  email: {
    type: DataTypes.STRING,
    allowNull: false,
    unique: true,
    validate: {
      isEmail: true
    }
  },
  password: {
    type: DataTypes.STRING,
    allowNull: false
  },
  type: {
    type: DataTypes.STRING,
    allowNull: false,
    defaultValue: 'CO',
    enum: ['A', 'C', 'W', 'CO'] //A = admin , W = mago, C = criatura, CO = recolector
  },
  isActive: {
    type: DataTypes.BOOLEAN,
    defaultValue: true
  }
}

import { UserModel } from '@services/database'

export const getAllUsers = async () => {
  try {
    return await UserModel.findAll()
  } catch (error) {
    throw error
  }
}

export const getAllUsersByType = async (type) => {
  try {
    return await UserModel.findAll({ where: { type } })
  } catch (error) {
    throw error
  }
}

export const getUserByUsername = async (username) => {
  try {
    const data = username ? String(username).toLowerCase() : null

    if (data) {
      return await UserModel.findOne({
        where: {
          username: data
        }
      })
    } else {
      return null
    }
  } catch (error) {
    throw error
  }
}

export const getUserByEmail = async (email) => {
  try {
    email = email.toLowerCase()

    return await UserModel.findOne({
      where: {
        email
      }
    })
  } catch (error) {
    throw error
  }
}

export const addUser = async (user) => {
  try {
    user.username = user.username.toLowerCase()
    user.email = user.email.toLowerCase()

    return (await UserModel.create(user)).dataValues
  } catch (error) {
    throw error
  }
}

export const updateUserByUname = async (username, user) => {
  try {
    await UserModel.update(user, { where: { username: username } })
    return await UserModel.findByPk(user.username)
  } catch (error) {
    throw error
  }
}

export const deleteUserByUname = async (username) => {
  try {
    const user = await UserModel.findByPk(username)

    return await user.update({ isActive: false })
  } catch (error) {
    throw error
  }
}

export const reactivateUserByUname = async (username) => {
  try {
    const user = await UserModel.findByPk(username)

    return await user.update({ isActive: true })
  } catch (error) {
    throw error
  }
}

export const updatePwd = async (username, newPassword) => {
  try {
    const user = await UserModel.findByPk(username)
    return await user.update({ password: newPassword })
  } catch (error) {
    throw error
  }
}

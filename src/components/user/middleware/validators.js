import bcrypt from 'bcryptjs'
import { check, validationResult } from 'express-validator'
import { message } from '@config/message'
import { handleResponse, handleError } from '@middleware/errorHandlers'

import { getUserByUsername, getUserByEmail } from '../dao'

import { getAllWizardByUsername } from '../../wizard/dao'

const message_request = 'es requerido'

export const verifyUserInUsers = async (req, res, next) => {
  const { id, type, username, creatureId, roleId } = req.token

  if (req.params?.username === username || type === 'A') {
    next()
  } else if (type === 'C' || roleId === 1) {
    const user = getUserByUsername(req.params.username)
    const creature = user.creatureId

    if (creature === id || creature === creatureId) next()
    else handleResponse(res, 403, message.forbidden_long)
  } else handleResponse(res, 403, message.forbidden_long)
}

export const userRequest = async (req, res, next) => {
  await check('username', `username ${message_request}`).notEmpty().run(req)
  await check('email', `email ${message_request}`).notEmpty().run(req)
  await check('password', `password ${message_request}`).notEmpty().run(req)

  const request = validationResult(req)
  if (!request.isEmpty()) {
    return res.status(422).json(request)
  } else {
    next()
  }
}

export const userUpdateRequest = async (req, res, next) => {
  await check('username', `username ${message_request}`).notEmpty().run(req)
  await check('email', `email ${message_request}`).notEmpty().run(req)
  // await check('type', `type ${message_request}`).notEmpty().run(req)

  const request = validationResult(req)
  if (!request.isEmpty()) {
    return res.status(422).json(request)
  } else {
    next()
  }
}

export const userExistRequest = async (req, res, next) => {
  const user = await getUserByUsername(req.params.username)

  if (!user) {
    handleResponse(res, 400, message.not_found_long)
  } else {
    next()
  }
}

export const userTypeExistRequest = async (req, res, next) => {
  if (!['A', 'C', 'W', 'CO'].includes(req.params.type)) {
    handleResponse(
      res,
      422,
      'Este tipo de usuario no se encuentra registrado en la BD'
    )
  } else {
    next()
  }
}

export const userNotExistRequest = async (req, res, next) => {
  const user = await getUserByUsername(req.body.username)

  if (user) {
    handleResponse(res, 400, message.duplicate_user)
  } else {
    next()
  }
}

export const userEmailNotExistRequest = async (req, res, next) => {
  const user = await getUserByEmail(req.body.email)

  if (user) {
    handleResponse(res, 400, message.duplicate_email)
  } else {
    next()
  }
}

export const updateUserEmailNotExistRequest = async (req, res, next) => {
  const user = await getUserByUsername(req.params.username)

  if (user.email != req.body.email) {
    const userEmail = await getUserByEmail(req.body.email)
    if (userEmail) {
      handleResponse(res, 400, message.duplicate_email)
    } else {
      next()
    }
  } else {
    next()
  }
}

export const updateUserUsernameNotExistRequest = async (req, res, next) => {
  if (req.params.username != req.body.username) {
    const userNewUsername = await getUserByUsername(req.body.username)
    if (userNewUsername) {
      handleResponse(res, 400, message.duplicate_user)
    } else {
      next()
    }
  } else {
    next()
  }
}

export const authDeleteRequest = async (req, res, next) => {
  const token = req.token
  const usernameToDelete = req.params.username
  switch (token.type) {
    case 'A':
      next()
      break

    case 'CO':
      if (usernameToDelete == token.username) {
        next()
      } else {
        handleError('access denied', res)
      }
      break

    case 'W':
      if (token.roleId === 1) {
        if (token.username === usernameToDelete) {
          next()
        }
        const myCreature = token.creatureId
        const wizardToDelete = await getAllWizardByUsername(usernameToDelete)
        const wizardCreatureToDelete = wizardToDelete?.creature.id

        if (myCreature === wizardCreatureToDelete) {
          next()
        } else {
          handleError('access denied', res)
        }
      } else {
        handleError('access denied', res)
      }
  }
}

export const updateMyPasswordRequest = async (req, res, next) => {
  await check('currentPassword', `currentPassword ${message_request}`)
    .notEmpty()
    .run(req)
  await check('newPassword', `newPassword ${message_request}`)
    .notEmpty()
    .run(req)
  await check('confirmPassword', `confirmPassword ${message_request}`)
    .notEmpty()
    .run(req)

  const request = validationResult(req)
  if (!request.isEmpty()) {
    return res.status(422).json(request)
  } else {
    if (req.params.username && req.token.username != req.params.username) {
      return handleError('access denied', res)
    }

    const username = req.params.username || req.token.username
    const myUser = await getUserByUsername(username)

    bcrypt.compare(req.body.currentPassword, myUser.password, (err, data) => {
      if (err) {
        handleError(err, res)
      } else if (data) {
        if (req.body.newPassword === req.body.confirmPassword) {
          next()
        } else {
          return handleError('Las contraseñas no coinciden', res)
        }
      } else {
        return handleError('Contraseña actual incorrecta', res)
      }
    })
  }
}

export const updatePasswordRequest = async (req, res, next) => {
  await check('password', `password ${message_request}`).notEmpty().run(req)

  const request = validationResult(req)
  if (!request.isEmpty()) {
    return res.status(422).json(request)
  } else {
    next()
  }
}

export const validatePassword = async (req, res, next) => {
  if (req.body.password) {
    if (!/(?=.*[A-Z])(?=.*[a-z])\S{8,}$/.test(req.body.password))
      return res.status(422).json({
        errors: [
          { msg: 'invalid password', param: 'password', location: 'body' }
        ]
      })
  }
  if (req.body.newPassword) {
    if (!/(?=.*[A-Z])(?=.*[a-z])\S{8,}$/.test(req.body.newPassword))
      return res.status(422).json({
        errors: [
          { msg: 'invalid password', param: 'password', location: 'body' }
        ]
      })
  }
  next()
}

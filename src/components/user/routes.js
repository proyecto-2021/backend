import { Router } from 'express'
import {
  findUserByUsername,
  listUsers,
  registerUser,
  updateUserByUsername,
  deleteUserByUsername,
  reactivateUserByUsername,
  updateMyPassword,
  updatePassword,
  listUsersByType,
  findProfileByUsername
} from './controller'
import {
  updateUserEmailNotExistRequest,
  updateUserUsernameNotExistRequest,
  userEmailNotExistRequest,
  userExistRequest,
  userNotExistRequest,
  userRequest,
  userUpdateRequest,
  authDeleteRequest,
  updateMyPasswordRequest,
  updatePasswordRequest,
  validatePassword,
  verifyUserInUsers,
  userTypeExistRequest
} from './middleware/validators'
import { authUser, authAdmin } from '@middleware/auth'

const router = Router()

router.get('/', authAdmin, listUsers)
router.get('/type/:type', [authAdmin, userTypeExistRequest], listUsersByType)
router.get('/profile', authUser, findProfileByUsername)
router.get(
  '/:username',
  [authUser, userExistRequest, verifyUserInUsers],
  findUserByUsername
)
router.post(
  '/',
  [
    authAdmin,
    userRequest,
    userNotExistRequest,
    userEmailNotExistRequest,
    validatePassword
  ],
  registerUser
)
router.put(
  '/:username',
  [
    authUser,
    userUpdateRequest,
    verifyUserInUsers,
    userExistRequest,
    updateUserEmailNotExistRequest,
    updateUserUsernameNotExistRequest,
    validatePassword
  ],
  updateUserByUsername
)
router.delete(
  '/:username',
  [authUser, userExistRequest, verifyUserInUsers, authDeleteRequest],
  deleteUserByUsername
)
router.patch(
  '/updateMyPassword',
  [authUser, updateMyPasswordRequest, validatePassword],
  updatePassword
)
router.patch(
  '/updatePassword/:username',
  [authAdmin, userExistRequest, updatePasswordRequest, validatePassword],
  updatePassword
)
router.patch(
  '/:username',
  [authAdmin, userExistRequest],
  reactivateUserByUsername
)

export const userRoutes = router

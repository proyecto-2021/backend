export const userResource = (resource) => ({
    username: resource.username,
    firstName: resource.firstName,
    lastName: resource.lastName,
    email: resource.email,
    password: resource.password,
    type: resource.type,
    isActive: resource.isActive,
  })
  
  export const userNoPasswordResource = (resource) => ({
    username: resource.username,
    firstName: resource.firstName,
    lastName: resource.lastName,
    email: resource.email,
    type: resource.type,
    isActive: resource.isActive,
  })

export const usersResource = (resources) =>
resources.map((resource) => userResource(resource))
  
export const usersNoPasswordResource = (resources) =>
resources.map((resource) => userNoPasswordResource(resource))
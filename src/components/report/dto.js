import { lookup } from 'geoip-lite'

export const reportResource = (resource) => ({
  id: resource.id,
  date: resource.createdAt,
  type: resource.type,
  description: resource.description,
  solution: resource.solution,
  image: resource.image,
  assignedAt: resource.assignedAt,
  endsAt: resource.endsAt,
  status: resource.status,
  classificationReport: resource.classificationReport,
  collector: resource.collector || resource.collectorId,
  creatureVersion: resource.creatureVersion || resource.collectorId,
  subMission: resource.subMission || resource.subMissionId,
  wizard: resource.wizard || resource.wizardId,
  geolocation: lookup(resource.ip) || null
})

export const reportResourceAlternative = (resource) => ({
  id: resource.id,
  date: resource.createdAt,
  type: resource.type,
  description: resource.description,
  solution: resource.solution,
  image: resource.image,
  assignedAt: resource.assignedAt,
  endsAt: resource.endsAt,
  deliveryAt: resource.deliveryAt,
  status: resource.status,
  classificationReport: resource.classificationReport,
  collector: resource.collector.dataValues,
  creatureVersion: resource.creatureVersion.dataValues,
  subMission: resource.subMission.dataValues,
  wizard: resource.wizard.dataValues,
  geolocation: lookup(resource.ip) || null
})

export const reportResourceWithoutCollector = (resource) => ({
  id: resource.id,
  date: resource.createdAt,
  collectorId: resource.collectorId,
  creatureVersionId: resource.creatureVersionId,
  subMissionId: resource.subMissionId,
  wizardId: resource.wizardId,
  classificationReport: resource.classificationReport,
  type: resource.type,
  description: resource.description,
  solution: resource.solution,
  assignedAt: resource.assignedAt,
  endsAt: resource.endsAt,
  deliveryAt: resource.deliveryAt,
  image: resource.image,
  status: resource.status,
  geolocation: lookup(resource.ip) || null
})

export const reportsResource = (resources) =>
  resources.map((resource) => reportResource(resource))

export const reportsResourceAlternative = (resources) =>
  resources.map((resource) => reportResourceAlternative(resource))

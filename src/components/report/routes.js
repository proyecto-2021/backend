import { sendUploadsToGCS } from '@middleware/uploadGCS'
import { uploadInMemoryImages } from '@middleware/uploadImages'
import { Router } from 'express'
import {
  findReportById,
  registerReport,
  listReports,
  listReportsByCollectorId,
  updateReport,
  deleteReport,
  listReportsByWizardId,
  updateReportClassificationReport,
  updateReportWizardId,
  updateReportStatus
} from './controller'
import {
  reportRequest,
  reportExistRequest,
  creatureVersionIdReportExistRequest,
  collectorIdReportExistRequest,
  verifyUserInReport,
  wizardIdReportExistRequest,
  reportRequestClassificationReport,
  reportRequestWizardId,
  reportIsAlreadyClasified
} from './middleware/validators'

const router = Router()

router.get('/', [verifyUserInReport], listReports)

router.get(
  '/collector/:collectorId',
  [verifyUserInReport, collectorIdReportExistRequest],
  listReportsByCollectorId
)

router.get(
  '/wizard/:wizardId',
  [verifyUserInReport, wizardIdReportExistRequest],
  listReportsByWizardId
)

router.get('/:id', [verifyUserInReport, reportExistRequest], findReportById)

router.post(
  '/',

  uploadInMemoryImages({ type: 'any' }),
  [
    verifyUserInReport,
    reportRequest,
    creatureVersionIdReportExistRequest,
    collectorIdReportExistRequest
  ],
  sendUploadsToGCS(),
  registerReport
)

router.put(
  '/:id',
  [verifyUserInReport, reportExistRequest, reportRequest],
  updateReport
)

router.delete('/:id', [verifyUserInReport, reportExistRequest], deleteReport)

router.patch(
  '/classificationReport/:id',
  [reportExistRequest, reportRequestClassificationReport],
  updateReportClassificationReport
)
router.patch(
  '/wizarId/:id',
  [reportExistRequest, reportIsAlreadyClasified, reportRequestWizardId],
  updateReportWizardId
)

router.patch('/status/:id', [reportExistRequest], updateReportStatus)

router.delete('/:id', [verifyUserInReport, reportExistRequest], deleteReport)
export const reportRoutes = router

import {
  db,
  CollectorModel,
  WizardModel,
  SubMissionModel
} from '@services/database'
import { ReportModel } from '@services/database'
import { CreatureVersionModel } from '@services/database'
import { Op } from 'sequelize'

export const getAllReports = async () => {
  try {
    return await ReportModel.findAll({
      include: [
        {
          model: CreatureVersionModel,
          as: 'creatureVersion'
        },
        {
          model: CollectorModel,
          as: 'collector'
        },
        {
          model: SubMissionModel,
          as: 'subMission'
        },
        {
          model: WizardModel,
          as: 'wizard'
        }
      ]
    })
  } catch (error) {
    throw error
  }
}

export const getAllReportsByCollectorId = async (collectorId) => {
  try {
    return await ReportModel.findAll({
      include: [
        {
          model: CreatureVersionModel,
          as: 'creatureVersion'
        },
        {
          model: CollectorModel,
          as: 'collector'
        },
        {
          model: SubMissionModel,
          as: 'subMission'
        },
        {
          model: WizardModel,
          as: 'wizard'
        }
      ],
      where: {
        collectorId
      },
      order: [['id', 'DESC']]
    })
  } catch (error) {
    throw error
  }
}

export const getAllApprovedReportsByCollectorId = async (collectorId) => {
  try {
    return await ReportModel.findAndCountAll({
      where: {
        collectorId: collectorId,
        status: 'PO'
      }
    })
  } catch (error) {
    throw error
  }
}

export const getAllReportsByCreature = async (id) => {
  try {
    return await ReportModel.findAll({
      include: [
        {
          model: CreatureVersionModel,
          as: 'creatureVersion'
        },
        {
          model: CollectorModel,
          as: 'collector'
        },
        {
          model: SubMissionModel,
          as: 'subMission'
        },
        {
          model: WizardModel,
          as: 'wizard'
        }
      ],
      where: {
        '$creatureVersion.creatureId$': id
      }
    })
  } catch (error) {
    throw error
  }
}

export const getAllReportsByWizard = async (wizardId) => {
  try {
    return await ReportModel.findAll({
      include: [
        {
          model: CreatureVersionModel,
          as: 'creatureVersion'
        },
        {
          model: CollectorModel,
          as: 'collector'
        },
        {
          model: SubMissionModel,
          as: 'subMission'
        },
        {
          model: WizardModel,
          as: 'wizard'
        }
      ],
      where: {
        wizardId
      }
    })
  } catch (error) {
    throw error
  }
}

export const getReportById = async (id) => {
  try {
    return await ReportModel.findByPk(id, {
      include: [
        {
          model: CreatureVersionModel,
          as: 'creatureVersion'
        },
        {
          model: CollectorModel,
          as: 'collector'
        },
        {
          model: SubMissionModel,
          as: 'subMission'
        },
        {
          model: WizardModel,
          as: 'wizard'
        }
      ]
    })
  } catch (error) {
    throw error
  }
}

export const getCountReportsDelayed = async () => {
  try {
    return await ReportModel.count({
      where: {
        deliveryAt: {
          [Op.gt]: db.Sequelize.col('endsAt')
        }
      }
    })
  } catch (error) {
    throw error
  }
}

export const getCountReportsDelayedWithCreatureId = async (creatureId) => {
  try {
    return await ReportModel.count({
      where: {
        deliveryAt: {
          [Op.gt]: db.Sequelize.col('endsAt')
        }
      },
      include: {
        model: CreatureVersionModel,
        as: 'creatureVersion',
        where: {
          creatureId
        }
      }
    })
  } catch (error) {
    throw error
  }
}

export const getCountReportsDelayedWithWizardId = async (wizardId) => {
  try {
    return await ReportModel.count({
      where: {
        deliveryAt: {
          [Op.gt]: db.Sequelize.col('endsAt')
        },
        wizardId
      }
    })
  } catch (error) {
    throw error
  }
}

export const addReport = async (report) => {
  try {
    return await ReportModel.create(report)
  } catch (error) {
    throw error
  }
}

export const updateReportById = async (id, report) => {
  try {
    const {
      missionId,
      wizardId,
      classificationReport,
      type,
      description,
      image,
      ends,
      solution,
      status
    } = report
    const Report = await ReportModel.findByPk(id, {
      include: [
        {
          model: CreatureVersionModel,
          as: 'creatureVersion'
        },
        {
          model: CollectorModel,
          as: 'collector'
        },
        {
          model: SubMissionModel,
          as: 'subMission'
        },
        {
          model: WizardModel,
          as: 'wizard'
        }
      ]
    })
    return Report.update({
      missionId,
      wizardId,
      classificationReport,
      type,
      description,
      image,
      ends,
      solution,
      status
    })
  } catch (error) {
    throw error
  }
}

export const updateClassificationReportById = async (
  id,
  classificationReport
) => {
  try {
    const Report = await ReportModel.findByPk(id, {
      include: [
        {
          model: CreatureVersionModel,
          as: 'creatureVersion'
        },
        {
          model: CollectorModel,
          as: 'collector'
        },
        {
          model: SubMissionModel,
          as: 'subMission'
        },
        {
          model: WizardModel,
          as: 'wizard'
        }
      ]
    })
    return await Report.update({ classificationReport })
  } catch (error) {
    throw error
  }
}

export const updateWizardIdById = async (id, { wizardId, endsAt }) => {
  try {
    const Report = await ReportModel.findByPk(id, {
      include: [
        {
          model: CreatureVersionModel,
          as: 'creatureVersion'
        },
        {
          model: CollectorModel,
          as: 'collector'
        },
        {
          model: SubMissionModel,
          as: 'subMission'
        },
        {
          model: WizardModel,
          as: 'wizard'
        }
      ]
    })
    const date = new Date()
    return await Report.update({ wizardId, assignedAt: date, endsAt })
  } catch (error) {
    throw error
  }
}

export const updateStatus = async (id, { status, solution }) => {
  try {
    const Report = await ReportModel.findByPk(id, {
      include: [
        {
          model: CreatureVersionModel,
          as: 'creatureVersion'
        },
        {
          model: CollectorModel,
          as: 'collector'
        },
        {
          model: SubMissionModel,
          as: 'subMission'
        },
        {
          model: WizardModel,
          as: 'wizard'
        }
      ]
    })

    if (status === 'PB') {
      const deliveryAt = new Date()
      return await Report.update({ status, solution, deliveryAt })
    } else return await Report.update({ status })
  } catch (error) {
    throw error
  }
}

export const deleteReportById = async (id) => {
  try {
    const Report = await ReportModel.findByPk(id, {
      include: [
        {
          model: CreatureVersionModel,
          as: 'creatureVersion'
        },
        {
          model: CollectorModel,
          as: 'collector'
        },
        {
          model: SubMissionModel,
          as: 'subMission'
        },
        {
          model: WizardModel,
          as: 'wizard'
        }
      ]
    })
    const data = await Report.destroy({
      where: {
        id
      }
    })

    return data
  } catch (error) {
    throw error
  }
}

export const countAllErrors = async () => {
  try {
    return await ReportModel.count({
      where: {
        type: 'E'
      }
    })
  } catch (error) {
    throw error
  }
}

export const countErrorsByCreature = async (creatureId) => {
  try {
    const query = `SELECT COUNT(public."creatureVersions"."creatureId")
    FROM public."reports", public."creatureVersions"
    WHERE public."reports"."type"='E'
    AND public."creatureVersions"."id" = public."reports"."creatureVersionId"
    AND public."creatureVersions"."creatureId" = ${creatureId}`
    const rsp = parseInt((await db.query(query))[0][0].count)
    return rsp
  } catch (error) {
    throw error
  }
}

export const countErrorsByWizard = async (wizardId) => {
  try {
    return await ReportModel.count({
      where: {
        wizardId,
        type: 'E'
      }
    })
  } catch (error) {
    throw error
  }
}

export const countAllImprovements = async () => {
  try {
    return await ReportModel.count({
      where: {
        type: 'I'
      }
    })
  } catch (error) {
    throw error
  }
}

export const countImprovementsByCreature = async (creatureId) => {
  try {
    const query = `SELECT COUNT(public."creatureVersions"."creatureId")
    FROM public."reports", public."creatureVersions"
    WHERE public."reports"."type"='I'
    AND public."creatureVersions"."id" = public."reports"."creatureVersionId"
    AND public."creatureVersions"."creatureId" = ${creatureId}`
    const rsp = parseInt((await db.query(query))[0][0].count)
    return rsp
  } catch (error) {
    throw error
  }
}

export const countImprovementsByWizard = async (wizardId) => {
  try {
    return await ReportModel.count({
      where: {
        wizardId,
        type: 'I'
      }
    })
  } catch (error) {
    throw error
  }
}

import { getCollectorById } from '@components/collector/dao'
import { getCreatureById } from '@components/creature/dao'
import { getCreatureVersionById } from '@components/creatureVersion/dao'
import { getWizardById } from '@components/wizard/dao'
import { addNotification } from '@components/notification/dao'
import { message } from '@config/message'
import { handleError, handleResponse } from '@middleware/errorHandlers'
import {
  addReport,
  getAllReports,
  getAllReportsByCollectorId,
  getReportById,
  updateReportById,
  deleteReportById,
  getAllReportsByCreature,
  getAllReportsByWizard,
  updateClassificationReportById,
  updateWizardIdById,
  updateStatus
} from './dao'
import {
  reportResource,
  reportResourceWithoutCollector,
  reportsResource
} from './dto'
import { addMissionCollector } from '@components/missionCollector/dao'

// /v1/reports
export const listReports = async (req, res) => {
  try {
    let data
    req?.token?.creatureId
      ? (data = await getAllReportsByCreature(req.token?.creatureId))
      : (data = await getAllReports())

    handleResponse(res, 200, message.success_long, reportsResource(data))
  } catch (error) {
    handleError(error, res)
  }
}
// /v1/reports/collector/{id}:
export const listReportsByCollectorId = async (req, res) => {
  try {
    const data = await getAllReportsByCollectorId(req.params.collectorId)

    handleResponse(res, 200, message.success_long, reportsResource(data))
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/reports/wizard/{id}:
export const listReportsByWizardId = async (req, res) => {
  try {
    const wizardId = req.params.wizardId

    const data = await getAllReportsByWizard(wizardId)

    handleResponse(res, 200, message.success_long, reportsResource(data))
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/reports/{id}:
export const findReportById = async (req, res) => {
  try {
    const data = await getReportById(req.params.id)

    handleResponse(res, 200, message.success_long, reportResource(data))
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/reports
export const registerReport = async (req, res) => {
  try {
    const report = req.body
    const { type, id } = req.token

    if (type === 'CO') report.collectorId = id
    console.log(report)

    if (req.files.length > 0) {
      report.image = req.files[0].url
    }

    report.ip = req.headers['public-ip'] || req.ip

    const data = await addReport(report)

    handleResponse(
      res,
      200,
      message.create_success,
      reportResourceWithoutCollector(data)
    )
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/reports/{id}:
export const updateReport = async (req, res) => {
  try {
    const ID = req.params.id
    const REPORT = req.body
    const DATA = await updateReportById(ID, REPORT)
    handleResponse(res, 200, message.update, reportResource(DATA))
  } catch (error) {
    handleError(error, res)
  }
}

export const updateReportClassificationReport = async (req, res) => {
  try {
    const id = req.params.id
    const { classificationReport } = req.body
    const data = await updateClassificationReportById(id, classificationReport)
    handleResponse(res, 200, message.update, reportResource(data))
  } catch (error) {
    handleError(error, res)
  }
}

export const updateReportWizardId = async (req, res) => {
  try {
    const id = req.params.id
    let { wizardId, endsAt } = req.body

    if (!endsAt) {
      endsAt = new Date()
      endsAt.setDate(endsAt.getDate() + 7)
    }

    const data = await updateWizardIdById(id, { wizardId, endsAt })
    handleResponse(res, 200, message.update, reportResource(data))
  } catch (error) {
    handleError(error, res)
  }
}

export const updateReportStatus = async (req, res) => {
  try {
    const id = req.params.id
    const { status, solution } = req.body

    const report = await getReportById(id)

    const {
      classificationReport,
      creatureVersionId,
      type,
      wizard,
      collector,
      subMissionId
    } = report

    const wizardModel = wizard.dataValues
    const collectorModel = collector.dataValues

    const scoreToCollectorOrWizard = {
      L: 20,
      M: 30,
      H: 40
    }

    const addCreatureByError = {
      L: 0.25,
      M: 0.5,
      H: 1
    }

    const addCreatureByMejora = {
      L: 0.1,
      M: 0.25,
      H: 0.5
    }

    if (status === 'PR') {
      let addScoreToCollector = scoreToCollectorOrWizard[classificationReport]

      const findCollector = await getCollectorById(collectorModel.id)
      findCollector.score += addScoreToCollector
      findCollector.stars = parseInt(findCollector.score / 100)
      await findCollector.save()

      let findCreatureVersionId = await getCreatureVersionById(
        creatureVersionId
      )
      const creatureId =
        findCreatureVersionId?.dataValues?.creature?.dataValues?.id
      let findCreature = await getCreatureById(creatureId)

      if (type === 'E') {
        const addCreatureScoreByError = addCreatureByError[classificationReport]
        findCreature.life += addCreatureScoreByError
        await findCreature.save()
      } else {
        const addCreatureScoreByMejora =
          addCreatureByMejora[classificationReport]
        findCreature.life += addCreatureScoreByMejora
        await findCreature.save()
      }

      //solo necesito el collectorId, luego crear el Json de forma automatica
      const notification = {
        collectorId: collectorModel.id,
        name: 'Estatus del Reporte',
        description: 'Se ha cambiado el estado de tu reporte a: en proceso'
      }
      await addNotification(notification)

      if (subMissionId)
        await addMissionCollector({
          subMissionId,
          collectorId: collector.id
        })
    } else if (status === 'PO') {
      const addScoreToWizard = scoreToCollectorOrWizard[classificationReport]

      const findWizard = await getWizardById(wizardModel.id)
      findWizard.score += addScoreToWizard
      await findWizard.save()

      let findCreatureVersionId = await getCreatureVersionById(
        creatureVersionId
      )
      const creatureId =
        findCreatureVersionId?.dataValues?.creature?.dataValues?.id
      let findCreature = await getCreatureById(creatureId)

      if (type === 'E') {
        const addCreatureScoreByError = addCreatureByError[classificationReport]
        findCreature.life -= addCreatureScoreByError
        await findCreature.save()
      } else {
        const addCreatureScoreByMejora =
          addCreatureByMejora[classificationReport]
        findCreature.life -= addCreatureScoreByMejora
        await findCreature.save()
      }

      const notification = {
        collectorId: collectorModel.id,
        name: 'Estatus del Reporte',
        description: 'Se ha cambiado el estado de tu reporte a: procesado'
      }
      await addNotification(notification)
    } else if (status === 'PB') {
      const notification = {
        collectorId: collectorModel.id,
        name: 'Estatus del Reporte',
        description: 'Se ha cambiado el estado de tu reporte a: en pruebas'
      }
      await addNotification(notification)
    } else if (status === 'R') {
      const notification = {
        collectorId: collectorModel.id,
        name: 'Estatus del Reporte',
        description: 'Se ha cambiado el estado de tu reporte a: rechazado'
      }
      await addNotification(notification)
    }

    const data = await updateStatus(id, { status, solution })

    handleResponse(res, 200, message.update, reportResource(data))
  } catch (error) {
    handleError(error, res)
  }
}

// /v1/reports/{id}:
export const deleteReport = async (req, res) => {
  try {
    const data = await deleteReportById(req.params.id)

    handleResponse(
      res,
      200,
      message.delete,
      reportResourceWithoutCollector(data)
    )
  } catch (error) {
    handleError(error, res)
  }
}

import { DataTypes } from 'sequelize'
import { v4 as uuidv4 } from 'uuid'

export const dataReport = {
  // id: {
  //   type: DataTypes.UUID,
  //   primaryKey: true,
  //   defaultValue: uuidv4()
  // },
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  collectorId: {
    type: DataTypes.INTEGER
  },
  creatureVersionId: {
    type: DataTypes.INTEGER,
    allowNull: false
  },
  subMissionId: {
    type: DataTypes.INTEGER
  },
  wizardId: {
    type: DataTypes.INTEGER
  },
  classificationReport: {
    type: DataTypes.STRING,
    enum: ['L', 'M', 'H'] //L = Baja , M = media, H = alta
  },
  // classificationReportId: {
  //   type: DataTypes.INTEGER
  // },
  type: {
    type: DataTypes.STRING,
    allowNull: false,
    defaultValue: 'E', //E = error , I = mejora
    enum: ['E', 'I']
  },
  description: {
    type: DataTypes.STRING,
    allowNull: false
  },
  solution: {
    type: DataTypes.STRING
  },
  image: {
    type: DataTypes.STRING
  },
  ip: {
    type: DataTypes.STRING
  },
  createdAt: {
    type: DataTypes.DATEONLY,
    defaultValue: DataTypes.NOW
  },
  assignedAt: {
    type: DataTypes.DATEONLY
  },
  endsAt: {
    type: DataTypes.DATEONLY
  },
  deliveryAt: {
    type: DataTypes.DATEONLY
  },
  status: {
    type: DataTypes.STRING,
    allowNull: false,
    defaultValue: 'P', //P = pendiente , PR = proceso, PB = pruebas, PO = procesado y R = Rechazado
    enum: ['P', 'PR', 'PB', 'PO', 'R']
  }
}

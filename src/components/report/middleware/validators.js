import { check, validationResult } from 'express-validator'
import { message } from '@config/message'
import { handleError, handleResponse } from '@middleware/errorHandlers'
import { getCreatureVersionById } from '@components/creatureVersion/dao'
import { getCollectorById } from '@components/collector/dao'
import { getReportById } from '../dao'
import { getWizardById } from '@components/wizard/dao'

const message_request = 'es requerido'

export const verifyUserInReport = async (req, res, next) => {
  const { type, roleId, creatureId, id } = req.token
  const { method } = req

  if (type === 'A') {
    next()
  } else if (type === 'CO') {
    if (method === 'POST') {
      next()
    } else if (req.params.id && method === 'GET') {
      ///////
      const report = await getReportById(req.params.id)

      if (report?.collectorId === id) {
        next()
      } else {
        handleResponse(res, 403, message.forbidden_long)
      }
      ///////
    } else if (req.params.collectorId) {
      ///////
      if (req.params.collectorId == id) {
        next()
      } else {
        handleResponse(res, 403, message.forbidden_long)
      }
      //////
    } else {
      handleResponse(res, 403, message.forbidden_long)
    }
  } else if (roleId === 1) {
    if (!req.params.id && !req.params.collectorId) {
      next()
    } else if (req.params.id) {
      ///////
      const report = await getReportById(req.params.id)
      const creature = report.creatureVersion

      if (creature?.creatureId === creatureId) {
        next()
      } else {
        handleResponse(res, 403, message.forbidden_long)
      }
      ///////
    } else {
      handleResponse(res, 403, message.forbidden_long)
    }
  } else if (type === 'W') {
    if (req.params.id) {
      ///////
      const report = await getReportById(req.params.id)

      if (report?.wizardId === id && method === 'GET') {
        next()
      } else {
        handleResponse(res, 403, message.forbidden_long)
      }
      ///////
    } else if (req.params.wizardId) {
      ///////
      if (req.params.wizardId == id) {
        next()
      } else {
        handleResponse(res, 403, message.forbidden_long)
      }
      ///////
    } else {
      handleResponse(res, 403, message.forbidden_long)
    }
  } else {
    handleResponse(res, 403, message.forbidden_long)
  }
}

export const reportRequest = async (req, res, next) => {
  await check('creatureVersionId', `creatureVersionId ${message_request}`)
    .notEmpty()
    .run(req)
  await check('type', `type ${message_request}`).notEmpty().run(req)
  await check('description', `description ${message_request}`)
    .notEmpty()
    .run(req)

  const request = validationResult(req)
  if (!request.isEmpty()) {
    return res.status(422).json(request)
  } else {
    next()
  }
}

export const reportRequestClassificationReport = async (req, res, next) => {
  await check('classificationReport', `classificationReport ${message_request}`)
    .notEmpty()
    .isIn(['L', 'M', 'H'])
    .run(req)

  const request = validationResult(req)
  if (!request.isEmpty()) {
    return res.status(422).json(request)
  } else {
    next()
  }
}

export const reportRequestWizardId = async (req, res, next) => {
  await check('wizardId', `wizardId ${message_request}`)
    .notEmpty()
    .isNumeric()
    .run(req)

  const request = validationResult(req)
  if (!request.isEmpty()) {
    return res.status(422).json(request)
  } else {
    next()
  }
}

export const reportIsAlreadyClasified = async (req, res, next) => {
  try {
    const findReport = await getReportById(req.params.id)

    if (findReport?.classificationReport) {
      next()
    } else {
      handleResponse(res, 400, 'El report no está clasificado')
    }
  } catch (error) {
    handleError(error, res)
  }
}

export const reportExistRequest = async (req, res, next) => {
  const report = await getReportById(req.params.id)
  if (!report) {
    handleResponse(res, 400, message.not_found_long)
  } else {
    next()
  }
}

export const creatureVersionIdReportExistRequest = async (req, res, next) => {
  const creatureId = req.params.id || req.body.creatureVersionId
  const creatureVersion = await getCreatureVersionById(creatureId)

  if (!creatureVersion) {
    handleResponse(res, 400, `creatureVersionId: ${message.not_found_long}`)
  } else {
    next()
  }
}

export const collectorIdReportExistRequest = async (req, res, next) => {
  if (req.params.collectorId || req.body.collectorId) {
    const collectorId = req.params.collectorId || req.body.collectorId
    const collector = await getCollectorById(collectorId)

    if (!collector) {
      handleResponse(res, 400, `collectorId: ${message.not_found_long}`)
    } else {
      next()
    }
  } else {
    next()
  }
}

export const wizardIdReportExistRequest = async (req, res, next) => {
  const wizardId = req.params.wizardId || req.body.wizardId
  const wizard = await getWizardById(wizardId)

  if (!wizard) {
    handleResponse(res, 400, `wizardId: ${message.not_found_long}`)
  } else {
    next()
  }
}

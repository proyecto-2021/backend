import { Router } from 'express'

import { userRoutes } from '@components/user/routes'
import { creatureRoutes } from '@components/creature/routes'
import { creatureVersionRoutes } from '@components/creatureVersion/routes'
import { rejectedReportsRouter } from '@components/rejectedReport/routes'
import { misionRoutes } from '@components/mision/routes'
import { favoriteRoutes } from '@components/favorite/routes'
import { roleRoutes } from '@components/role/routes'
import { wizardRoutes } from '@components/wizard/routes'
import { taskRoutes } from '@components/task/routes'
import { collectorRoutes } from '@components/collector/routes'
// import { classificationReportRoutes } from '@components/classificationReport/routes'
import { reportRoutes } from '@components/report/routes'
import { authRoutes } from '@components/auth/routes'
import { authUser, authAdminOrCollector } from '@middleware/auth'
import { verifyUserInRole } from '@components/role/middleware/validators'
import { notificationRoutes } from '@components/notification/routes'
import { listReports } from '@components/report/controller'
import { categoryRoutes } from '@components/category/routes'
import { subMissionRoutes } from '@components/submission/routes'
import { missionCollectorsRoutes } from '@components/missionCollector/routes'
import { verifyUserInSubMission } from '@components/submission/middleware/validators'
import { dashboardRoutes } from '@components/dashboard/routes'
const router = Router()

router.get('/reports/all', listReports)

router.use('/creatures/versions', authUser, creatureVersionRoutes)
router.use('/creatures', authUser, creatureRoutes)
// router.use('/reports/classifications', classificationReportRoutes)
router.use('/reports/rejected', authUser, rejectedReportsRouter)
router.use('/reports', authUser, reportRoutes)
router.use('/misions', authUser, misionRoutes)
router.use('/favorites', authAdminOrCollector, favoriteRoutes)
router.use('/users', userRoutes)
router.use('/role', [authUser, verifyUserInRole], roleRoutes)
router.use('/wizard', authUser, wizardRoutes)
router.use('/task', authUser, taskRoutes)
router.use('/collectors', authUser, collectorRoutes)
router.use('/auth', authRoutes)
router.use('/notifications', authAdminOrCollector, notificationRoutes)
router.use('/rcollectors', authUser, collectorRoutes)
router.use('/categories', authUser, categoryRoutes)
router.use('/submissions', [authUser, verifyUserInSubMission], subMissionRoutes)
router.use('/missionCollectors', [authUser], missionCollectorsRoutes)
router.use('/dashboard', [authUser], dashboardRoutes)

export default router

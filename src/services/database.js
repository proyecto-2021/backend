import { Sequelize } from 'sequelize'
import { dataClassificationReport } from '@components/classificationReport/model'
import { dataCollector } from '@components/collector/model'
import { dataCreature } from '@components/creature/model'
import { dataCreatureVersion } from '@components/creatureVersion/model'
import { dataFavorite } from '@components/favorite/model'
import { dataMision } from '@components/mision/model'
import { dataRejectedReport } from '@components/rejectedReport/model'
import { dataReport } from '@components/report/model'
import { dataRole } from '@components/role/model'
import { dataTask } from '@components/task/model'
import { dataUser } from '@components/user/model'
import { dataWizard } from '@components/wizard/model'
import { dataNotification } from '@components/notification/model'
import { dataCategory } from '@components/category/model'
import { dataSubMission } from '@components/submission/model'
import { dataMissionCollector } from '@components/missionCollector/model'
require('dotenv').config()

// for heroku
export const db = new Sequelize(
  process.env.DB_NAME, //dbName
  process.env.DB_USERNAME, //dbUsername
  process.env.DB_PASSWORD, //dbPassword
  {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    dialect: 'postgres',
    dialectOptions: {
      ssl: {
        require: true,
        rejectUnauthorized: false
      }
    },
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000
    },
    logging: false
  }
)

// for heroku
// export const db = new Sequelize(
//   'agatha_db', //dbName
//   'agatha_user', //dbUsername
//   'vWPtTq46NoCctxib', //dbPassword
//   {
//     host: 'db-postgresql-nyc1-cryptopos-do-user-1487765-0.db.ondigitalocean.com',
//     port: 25060,
//     dialect: 'postgres',
//     dialectOptions: {
//       ssl: {
//         require: true,
//         rejectUnauthorized: false
//       }
//     },
//     pool: {
//       max: 5,
//       min: 0,
//       acquire: 30000,
//       idle: 10000
//     },
//     logging: false
//   }
// )

// for local
// export const db = new Sequelize(
//   'agatha', //dbName
//   'postgres', //dbUsername
//   'postgres', //dbPassword
//   {
//     host: 'localhost',
//     dialect: 'postgres',
//     pool: {
//       max: 5,
//       min: 0,
//       acquire: 30000,
//       idle: 10000
//     },
//     logging: false
//     //  sync: { force: false, alter: true }
//   }
// )

//Tables in DB
const User = db.define('user', dataUser, {
  timestamps: false
})
const Role = db.define('role', dataRole, {
  timestamps: false
})
const Creature = db.define('creature', dataCreature, {
  timestamps: false
})
const Wizard = db.define('wizard', dataWizard, {
  timestamps: false
})
const Collector = db.define('collector', dataCollector, {
  timestamps: false
})
const Favorite = db.define('favorite', dataFavorite, {
  timestamps: false
})
const CreatureVersion = db.define('creatureVersion', dataCreatureVersion, {
  timestamps: false
})
const Mision = db.define('mision', dataMision, {
  timestamps: false
})
const ClassificationReport = db.define(
  'classificationReport',
  dataClassificationReport,
  {
    timestamps: false
  }
)
const Report = db.define('report', dataReport, {
  timestamps: false
})
const RejectedReport = db.define('rejectedReport', dataRejectedReport, {
  timestamps: false
})
const Task = db.define('task', dataTask, {
  timestamps: false
})
const Notification = db.define('notification', dataNotification, {
  timestamps: false
})
const Category = db.define('category', dataCategory, {
  timestamps: false
})
const SubMission = db.define('submission', dataSubMission, {
  timestamps: false
})

const MissionCollector = db.define('missionCollector', dataMissionCollector, {
  timestamps: false
})

// Relationships between models
Role.hasMany(Wizard, { as: 'wizards' })
Creature.belongsTo(User, {
  foreignKey: 'username',
  as: 'user',
  onDelete: 'cascade'
})
Creature.belongsTo(Category, {
  foreignKey: 'categoryId',
  as: 'category'
})
Category.hasMany(Creature, { as: 'creatures' })
Creature.hasMany(Wizard, { as: 'wizards' })
Creature.hasMany(Favorite, { as: 'favorites' })
Creature.hasMany(CreatureVersion, { as: 'creatureVersions' })
Creature.hasMany(ClassificationReport, { as: 'classificationReports' })
Wizard.belongsTo(User, {
  foreignKey: 'username',
  as: 'user',
  onDelete: 'cascade'
})
Wizard.belongsTo(Role, { foreignKey: 'roleId', as: 'role' })
Wizard.belongsTo(Creature, { foreignKey: 'creatureId', as: 'creature' })
Wizard.hasMany(Report, { as: 'reports', onDelete: 'cascade' })
Wizard.hasMany(Task, { as: 'tasks', onDelete: 'cascade', hooks: true })
Wizard.hasMany(RejectedReport, { as: 'rejectedReports', onDelete: 'cascade' })
Collector.belongsTo(User, {
  foreignKey: 'username',
  as: 'user',
  onDelete: 'cascade'
})
Collector.hasMany(Favorite, { as: 'favorites' })
Collector.hasMany(Report, { as: 'reports' })
Collector.hasMany(Notification, { as: 'notifications' })
Collector.hasMany(MissionCollector, {
  as: 'missionCollectors',
  onDelete: 'cascade'
})
Favorite.belongsTo(Creature, { foreignKey: 'creatureId', as: 'creature' })
Favorite.belongsTo(Collector, { foreignKey: 'collectorId', as: 'collector' })
CreatureVersion.belongsTo(Creature, {
  foreignKey: 'creatureId',
  as: 'creature'
})
CreatureVersion.hasMany(Mision, { as: 'misions', onDelete: 'cascade' })
CreatureVersion.hasMany(Report, { as: 'reports', onDelete: 'cascade' })
ClassificationReport.belongsTo(Creature, {
  foreignKey: 'creatureId',
  as: 'creature'
})
// ClassificationReport.hasMany(Report, { as: 'reports' })
Mision.belongsTo(CreatureVersion, {
  foreignKey: 'creatureVersionId',
  as: 'creatureVersion'
})
Mision.hasMany(SubMission, { as: 'submission', onDelete: 'cascade' })
SubMission.belongsTo(Mision, {
  foreignKey: 'missionId',
  as: 'mission'
})
SubMission.hasMany(Report, { as: 'reports' })
SubMission.hasMany(MissionCollector, {
  as: 'missionCollectors',
  onDelete: 'cascade'
})
MissionCollector.belongsTo(Collector, {
  foreignKey: 'collectorId',
  as: 'collector'
})
MissionCollector.belongsTo(SubMission, {
  foreignKey: 'subMissionId',
  as: 'subMission'
})
Report.belongsTo(Collector, { foreignKey: 'collectorId', as: 'collector' })
Report.belongsTo(CreatureVersion, {
  foreignKey: 'creatureVersionId',
  as: 'creatureVersion'
})
Report.belongsTo(SubMission, { foreignKey: 'subMissionId', as: 'subMission' })
Report.belongsTo(Wizard, { foreignKey: 'wizardId', as: 'wizard' })
// Report.belongsTo(ClassificationReport, {
//   foreignKey: 'classificationReportId',
//   as: 'classificationReport'
// })
Report.hasMany(Task, { as: 'tasks', onDelete: 'cascade' })
RejectedReport.belongsTo(Report, { foreignKey: 'reportId', as: 'report' })
RejectedReport.belongsTo(Wizard, { foreignKey: 'wizardId', as: 'wizard' })
Task.belongsTo(Report, { foreignKey: 'reportId', as: 'report' })
Task.belongsTo(Wizard, { foreignKey: 'wizardId', as: 'wizard' })
Notification.belongsTo(Collector, {
  foreignKey: 'collectorId',
  as: 'collector',
  onDelete: 'cascade'
})

//Synchronizing all models at once
const syncModels = async () => {
  //await Sequelize.sync({ alter: true })
  try {
    // await User.sync({ alter: true })
    // await Role.sync({ alter: true })
    // await Creature.sync({ alter: true })
    // await Wizard.sync({ alter: true })
    // await Collector.sync({ alter: true })
    // await Favorite.sync({ alter: true })
    // await CreatureVersion.sync({ alter: true })
    // await Mision.sync({ alter: true })
    // await SubMission.sync({ alter: true })
    // await MissionCollector.sync({ alter: true })
    // await ClassificationReport.sync({ alter: true })
    // await Report.sync({ alter: true })
    // await RejectedReport.sync({ alter: true })
    // await Task.sync({ alter: true })
    // await Notification.sync({ alter: true })
    // await Category.sync({ alter: true })
  } catch (error) {
    console.log(error)
  }
}

syncModels()

export const UserModel = User
export const RoleModel = Role
export const CreatureModel = Creature
export const WizardModel = Wizard
export const CollectorModel = Collector
export const FavoriteModel = Favorite
export const CreatureVersionModel = CreatureVersion
export const MisionModel = Mision
export const SubMissionModel = SubMission
export const MissionCollectorModel = MissionCollector
export const ClassificationReportModel = ClassificationReport
export const ReportModel = Report
export const RejectedReportModel = RejectedReport
export const TaskModel = Task
export const NotificationModel = Notification
export const CategoryModel = Category

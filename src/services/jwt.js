import { message } from '@config/message'
import { handleError, handleResponse } from '@middleware/errorHandlers'
import jwt from 'jsonwebtoken'

export const generateJWT = (user) => {
  const { id, username, type, roleId, creatureId, interest } = user

  const token = jwt.sign(
    {
      id: id || username,
      username: username,
      type: type,
      roleId: roleId || '',
      creatureId: creatureId || '',
      categories: interest || null
    },
    'r=2%P%25TDMNBaC6'
  )

  return token
}

export const generateValidationToken = (user) => {
  const token = jwt.sign(
    {
      id: user.username,
      type: user.type
    },
    '6Cpv_9uH$U'
  )

  return token
}

export const decodeToken = (token) => {
  return jwt.verify(token, 'r=2%P%25TDMNBaC6')
}

export const verifyToken = (req, res) => {
  try {
    const { authorization } = req.headers
    if (authorization) {
      const bearer = authorization.split(' ')
      const bearerToken = bearer[1]

      jwt.verify(bearerToken, 'r=2%P%25TDMNBaC6', (err, data) => {
        if (err) {
          handleResponse(res, 403, 'invalid token')
          return null
        } else {
          return jwt.decode(bearerToken)
        }
      })
    } else {
      handleResponse(res, 401, message.unauthorized)
    }
  } catch (error) {
    handleError(error, res)
  }
}

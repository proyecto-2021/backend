# AGATHA 0.0.1

# Repositorio Backend. Sistemas de información 2021

Backend para el proyecto de sistemas de información. Proyecto que se realizará por los estudiantes del décimo semestre de ingeniería Informática de la UCLA. Este proyecto tiene como objetivo gestionar de forma administrativa la aplicación movil Agatha.


## Integrantes de Equipo

- Albert Acevedo (Líder de equipo)
- Gabriel Roa
- Ruben Gutierrez
- Eduardo Nieves

### Especificaciones técnicas / Referencias

- Documentación oficial `https://expressjs.com/` 

#### Requerimientos previos:

- NodeJS
- Npm

#### Branch

- master -> Production `https://agatha.najoconsultores.com/api/v1`
- develop -> Develop `https://agatha-develop.herokuapp.com/api/v1`

### Configurar el entorno de desarrollo

#### Comandos para desplegar funciones.

| °   | Comando         | Descripción                                      | Notas |
| --- | --------------- | ------------------------------------------------ | ----- |
| 1   | `npm run dev`   | Compilación de la aplicación para el desarrollo  |
| 2   | `npm run build` | Construcción de la aplicación para producción    |
| 3   | `npm run start` | Ejecutar la aplicación en producción             |

### Folder Structure

```
├──emails
|   ├──newpassword.pug
├──src:
|   ├──bin:
|   │   ├── www.js
|   ├──components:
|   │   ├── ...
|   ├──config:
|   │   ├── message.js
|   ├──middleware:
|   │   ├── ...
|   ├──routes:
|   │   ├── index.js
|   ├──services:
|   │   ├── ...
|   ├──router:
|   │   ├── ...
|   ├──services:
|   │   ├── ...
|   ├──app.js
├──.babelrc
├──.editorconfig
├──.eslintrc.json
├──.gitignore
├──.gitlab-ci.yml
├──.gitignore
├──agatha.json
├──jsconfig.json
├──prettier.config.js
├──package.json
├──package-lock.json
├──README.md
```

### Notas

- La aplicación fue creada por medio de express.
- Paquetes y dependecias utilizadas para la elaboración:

| °   | Paquete                            | Versión         |
| --- | ---------------------------------- | --------------- |
| 1   | `@google-cloud/storage`            | `^5.8.5`        |
| 2   | `bcryptjs`                         | `^2.4.3`        |
| 3   | `cors`                             | `^2.8.5`        |
| 4   | `dotenv`                           | `^10.0.0`       |
| 5   | `email-templates`                  | `^8.0.7`        |
| 6   | `express`                          | `^4.17.1`       |
| 7   | `express-validator`                | `^6.6.0`        |
| 8   | `fs-extra`                         | `^10.0.0`       |
| 9   | `jsonwebtoken`                     | `^8.5.1`        |
| 10  | `morgan`                           | `^1.10.0`       |
| 11  | `multer`                           | `^1.4.2`        |
| 12  | `nanoid`                           | `^3.1.23`       |
| 13  | `nodemailer`                       | `^6.6.1`        |
| 14  | `pg`                               | `^8.6.0`        |
| 15  | `pg-hstore`                        | `^2.3.3`        |
| 16  | `sequelize`                        | `^6.6.2`        |
| 17  | `swagger-jsdoc`                    | `6`             |
| 18  | `swagger-ui-express`               | `^4.1.6`        |
| 19  | `util`                             | `^0.12.4`       |
| 20  | `uuid`                             | `^8.3.2`        |

### Pruebas Swagger

```
https://agatha.najoconsultores.com/swagger/
```
